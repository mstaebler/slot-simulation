import { SlotSymbol } from '../SlotConfigurators/SlotSymbol';

export interface LineResult {
	symbol: SlotSymbol;
	matches: number;
}
