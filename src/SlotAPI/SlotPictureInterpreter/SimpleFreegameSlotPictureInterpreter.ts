import { PayLine } from './PayLine';
import { SlotReel } from '../SlotMachine/SlotReel';
import { SlotResult } from './SlotResult';
import { FreegameSlotPictureInterpreter } from './FreegameSlotPictureInterpreter';
import { SlotSymbol } from '../SlotConfigurators/SlotSymbol';

export abstract class SimpleFreegameSlotPictureInterpreter implements FreegameSlotPictureInterpreter {
	constructor(
		protected payLines: PayLine[],
		protected multiplierMap: { [key in string]: number[] },
        protected scatterSymbol: SlotSymbol,
        protected freeGamesMap: { [key in number]: number }
	) {}

	abstract interpretResult(spinResult: SlotReel[], buyIn: number): SlotResult;

	abstract interpretResultAsFreeGameSpin(
		spinResult: SlotReel[],
		buyIn: number,
		freeGameSymbol: SlotSymbol
	): SlotResult;
}
