import { ReelCoordinate } from '../SlotMachine/ReelCoordinate';

export interface PayLine {
	reelCoordinates: ReelCoordinate[];
}
