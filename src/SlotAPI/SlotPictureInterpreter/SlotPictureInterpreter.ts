import { SlotReel } from '../SlotMachine/SlotReel';
import { SlotResult } from './SlotResult';

export interface SlotPictureInterpreter {
	interpretResult(spinResult: SlotReel[], buyIn: number): SlotResult;
}
