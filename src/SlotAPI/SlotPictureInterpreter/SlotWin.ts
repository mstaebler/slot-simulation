import { SlotSymbol } from '../SlotConfigurators/SlotSymbol';

export interface SlotWin {
	winningLine: number;
	symbol: SlotSymbol;
	amount: number;
	matches: number;
}
