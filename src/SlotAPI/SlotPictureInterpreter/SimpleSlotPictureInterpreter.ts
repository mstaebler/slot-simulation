import { SlotPictureInterpreter } from './SlotPictureInterpreter';
import { PayLine } from './PayLine';
import { SlotReel } from '../SlotMachine/SlotReel';
import { SlotResult } from './SlotResult';

export abstract class SimpleSlotPictureInterpreter implements SlotPictureInterpreter {
	constructor(protected payLines: PayLine[], protected multiplierMap: { [key in string]: number[] }) {}

	abstract interpretResult(spinResult: SlotReel[], buyIn: number): SlotResult;
}
