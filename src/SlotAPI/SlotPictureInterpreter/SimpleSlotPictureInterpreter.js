"use strict";
exports.__esModule = true;
var SimpleSlotPictureInterpreter = /** @class */ (function () {
    function SimpleSlotPictureInterpreter(payLines, multiplierMap) {
        this.payLines = payLines;
        this.multiplierMap = multiplierMap;
    }
    return SimpleSlotPictureInterpreter;
}());
exports.SimpleSlotPictureInterpreter = SimpleSlotPictureInterpreter;
