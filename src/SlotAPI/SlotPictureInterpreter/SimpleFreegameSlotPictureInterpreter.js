"use strict";
exports.__esModule = true;
var SimpleFreegameSlotPictureInterpreter = /** @class */ (function () {
    function SimpleFreegameSlotPictureInterpreter(payLines, multiplierMap, scatterSymbol, freeGamesMap) {
        this.payLines = payLines;
        this.multiplierMap = multiplierMap;
        this.scatterSymbol = scatterSymbol;
        this.freeGamesMap = freeGamesMap;
    }
    return SimpleFreegameSlotPictureInterpreter;
}());
exports.SimpleFreegameSlotPictureInterpreter = SimpleFreegameSlotPictureInterpreter;
