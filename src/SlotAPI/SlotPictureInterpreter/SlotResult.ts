import { SlotWin } from './SlotWin';
import {SlotSymbol} from "../SlotConfigurators/SlotSymbol";

export interface SlotResult {
	payout: number;
	wins: SlotWin[];
	slotPicture: SlotSymbol[][];
}
