import { SlotPictureInterpreter } from './SlotPictureInterpreter';
import { SlotReel } from '../SlotMachine/SlotReel';
import { SlotResult } from './SlotResult';
import { SlotSymbol } from '../SlotConfigurators/SlotSymbol';

export interface FreegameSlotPictureInterpreter extends SlotPictureInterpreter {
	interpretResultAsFreeGameSpin(spinResult: SlotReel[], buyIn: number, freeGameSymbol: SlotSymbol): SlotResult;
}
