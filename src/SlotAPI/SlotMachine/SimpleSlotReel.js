"use strict";
exports.__esModule = true;
var SimpleSlotReel = /** @class */ (function () {
    function SimpleSlotReel(symbols, symbolPool) {
        this.symbols = symbols;
        this.symbolPool = symbolPool;
    }
    SimpleSlotReel.prototype.getSymbols = function () {
        return this.symbols;
    };
    SimpleSlotReel.prototype.getSpinResult = function () {
        var _this = this;
        var symbols = this.symbols.map(function (s) { return _this.symbolPool.drawNextSymbol(); });
        this.symbols = symbols;
        this.symbolPool.resetPool();
        return this;
    };
    SimpleSlotReel.prototype.toString = function () {
        var returnString = '';
        this.symbols.forEach(function (s) { return (returnString += s.name + ' '); });
        return returnString;
    };
    SimpleSlotReel.prototype.setSymbols = function (symbols) {
        this.symbols = symbols;
    };
    SimpleSlotReel.prototype.getSymbolPool = function () {
        return this.symbolPool;
    };
    return SimpleSlotReel;
}());
exports.SimpleSlotReel = SimpleSlotReel;
