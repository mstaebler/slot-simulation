import {SlotMachine} from "./SlotMachine";
import {SlotSymbol} from "../SlotConfigurators/SlotSymbol";

export interface FreegamesSlotMachine extends SlotMachine{
    drawFreegameSymbol(): SlotSymbol;
    addSymbol(symbol: SlotSymbol);
}