import { SlotReel } from './SlotReel';
import { SlotSymbol } from '../SlotConfigurators/SlotSymbol';
import { SymbolPool } from '../SlotConfigurators/SymbolPool';

export abstract class SimpleSlotReel implements SlotReel {
	constructor(private symbols: SlotSymbol[], private symbolPool: SymbolPool) {}

	public getSymbols(): SlotSymbol[] {
		return this.symbols;
	}

	public getSpinResult(): SimpleSlotReel {
		let symbols: SlotSymbol[] = this.symbols.map(s => this.symbolPool.drawNextSymbol());
		this.symbols = symbols;
		this.symbolPool.resetPool();
		return this;
	}

	public toString(): string {
		let returnString: string = '';
		this.symbols.forEach(s => (returnString += s.name + ' '));
		return returnString;
	}

	setSymbols(symbols: SlotSymbol[]) {
		this.symbols = symbols;
	}

	getSymbolPool(): SymbolPool {
		return this.symbolPool;
	}
}
