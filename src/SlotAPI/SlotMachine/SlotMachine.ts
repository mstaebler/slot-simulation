import { SlotReel } from './SlotReel';
import { SlotSymbol } from '../SlotConfigurators/SlotSymbol';

export interface SlotMachine {
	spin(): SlotReel[];
	getCurrentPicture(): SlotReel[];
	getReelSizes(): number[];
	toString(): string;
	setPicture(pictures: SlotSymbol[][]);
}
