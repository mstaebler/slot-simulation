import { SlotSymbol } from '../SlotConfigurators/SlotSymbol';

export interface SlotReel {
	getSymbols(): SlotSymbol[];
	getSpinResult(): SlotReel;
	toString(): string;
	setSymbols(symbols: SlotSymbol[]);
}
