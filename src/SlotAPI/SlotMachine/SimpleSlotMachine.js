"use strict";
exports.__esModule = true;
var SimpleSlotMachine = /** @class */ (function () {
    function SimpleSlotMachine(startConfiguration) {
        this.reels = startConfiguration;
        this.reelSizes = startConfiguration.map(function (reel) { return reel.getSymbols().length; });
    }
    SimpleSlotMachine.prototype.getCurrentPicture = function () {
        return this.reels;
    };
    SimpleSlotMachine.prototype.getReelSizes = function () {
        return this.reelSizes;
    };
    SimpleSlotMachine.prototype.toString = function () {
        var returnString = "";
        this.reels.forEach(function (r) { return (returnString += "[" + r.toString() + "] "); });
        return returnString;
    };
    return SimpleSlotMachine;
}());
exports.SimpleSlotMachine = SimpleSlotMachine;
