"use strict";
exports.__esModule = true;
var Configurations_1 = require("../../main/slot/Configurations");
var SimpleFreegameSlotMachine = /** @class */ (function () {
    function SimpleFreegameSlotMachine(startConfiguration, freegameSymbolProbability, symbolNameToSymbolMap) {
        this.freegameSymbolProbability = freegameSymbolProbability;
        this.symbolNameToSymbolMap = symbolNameToSymbolMap;
        this.reels = startConfiguration;
        this.reelSizes = startConfiguration.map(function (reel) { return reel.getSymbols().length; });
    }
    SimpleFreegameSlotMachine.prototype.getCurrentPicture = function () {
        return this.reels;
    };
    SimpleFreegameSlotMachine.prototype.getReelSizes = function () {
        return this.reelSizes;
    };
    SimpleFreegameSlotMachine.prototype.toString = function () {
        var returnString = '';
        this.reels.forEach(function (r) { return (returnString += '[' + r.toString() + '] '); });
        return returnString;
    };
    SimpleFreegameSlotMachine.prototype.drawFreegameSymbol = function () {
        var losTopf = [];
        for (var key in this.freegameSymbolProbability) {
            for (var i = 0; i < this.freegameSymbolProbability[key]; i++) {
                losTopf.push(this.symbolNameToSymbolMap[key]);
            }
        }
        return losTopf[Math.floor(Math.random() * losTopf.length)];
    };
    SimpleFreegameSlotMachine.prototype.setPicture = function (picture) {
        this.reels.forEach(function (reel, index) { return reel.setSymbols(picture[index]); });
    };
    SimpleFreegameSlotMachine.prototype.addSymbol = function (symbol) {
        var _this = this;
        Configurations_1.indexOfReelToAddSymbolInFreegames[symbol.name].forEach(function (i) {
            return _this.getCurrentPicture()[i].addSymbol(symbol);
        });
    };
    return SimpleFreegameSlotMachine;
}());
exports.SimpleFreegameSlotMachine = SimpleFreegameSlotMachine;
