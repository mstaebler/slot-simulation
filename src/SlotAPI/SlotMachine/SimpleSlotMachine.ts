import { SlotMachine } from './SlotMachine';
import { SlotReel } from './SlotReel';
import {SlotSymbol} from "../SlotConfigurators/SlotSymbol";

export abstract class SimpleSlotMachine implements SlotMachine {
	private reels: SlotReel[];
	private reelSizes: number[];

	constructor(startConfiguration: SlotReel[]) {
		this.reels = startConfiguration;
		this.reelSizes = startConfiguration.map(reel => reel.getSymbols().length);
	}

	public getCurrentPicture(): SlotReel[] {
		return this.reels;
	}

	public getReelSizes(): number[] {
		return this.reelSizes;
	}

	abstract spin(): SlotReel[];

	toString(): string {
		let returnString: string = '';
		this.reels.forEach(r => (returnString += '[' + r.toString() + '] '));
		return returnString;
	}

	setPicture(picture: SlotSymbol[][]) {
		this.reels.forEach((reel, index) => reel.setSymbols(picture[index]));
	}
}
