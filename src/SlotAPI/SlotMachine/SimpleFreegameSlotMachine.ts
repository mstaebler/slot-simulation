import { FreegamesSlotMachine } from './FreegamesSlotMachine';
import { SlotReel } from './SlotReel';
import { SlotSymbol } from '../SlotConfigurators/SlotSymbol';
import {MySlotReel} from "../../main/slot/MySlotReel";
import {indexOfReelToAddSymbolInFreegames} from "../../main/slot/Configurations";

export abstract class SimpleFreegameSlotMachine implements FreegamesSlotMachine {
	private reels: SlotReel[];
	private reelSizes: number[];

	constructor(
		startConfiguration: SlotReel[],
		private freegameSymbolProbability: { [key in string]: number },
		private symbolNameToSymbolMap: { [key in string]: SlotSymbol }
	) {
		this.reels = startConfiguration;
		this.reelSizes = startConfiguration.map(reel => reel.getSymbols().length);
	}

	public getCurrentPicture(): SlotReel[] {
		return this.reels;
	}

	public getReelSizes(): number[] {
		return this.reelSizes;
	}

	abstract spin(): SlotReel[];

	toString(): string {
		let returnString: string = '';
		this.reels.forEach(r => (returnString += '[' + r.toString() + '] '));
		return returnString;
	}

	drawFreegameSymbol(): SlotSymbol {
		let losTopf: SlotSymbol[] = [];
		for (let key in this.freegameSymbolProbability) {
			for (let i = 0; i < this.freegameSymbolProbability[key]; i++) {
				losTopf.push(this.symbolNameToSymbolMap[key]);
			}
		}
		return losTopf[Math.floor(Math.random() * losTopf.length)];
	}

	setPicture(picture: SlotSymbol[][]) {
		this.reels.forEach((reel, index) => reel.setSymbols(picture[index]));
	}

	addSymbol(symbol: SlotSymbol) {
		indexOfReelToAddSymbolInFreegames[symbol.name].forEach(i =>
			(this.getCurrentPicture()[i] as MySlotReel).addSymbol(symbol)
		);
	}
}
