"use strict";
exports.__esModule = true;
var SimpleSymbolPool = /** @class */ (function () {
    function SimpleSymbolPool(symbolPool) {
        this.symbolPool = symbolPool;
    }
    SimpleSymbolPool.prototype.resetPool = function () {
        this.currentPool = Object.assign([], this.symbolPool);
    };
    return SimpleSymbolPool;
}());
exports.SimpleSymbolPool = SimpleSymbolPool;
