import { SymbolPool } from './SymbolPool';
import { SlotSymbol } from './SlotSymbol';

export abstract class SimpleSymbolPool implements SymbolPool {
	abstract currentPool: SlotSymbol[];

	constructor(private symbolPool: SlotSymbol[]) {}

	abstract drawNextSymbol(): SlotSymbol;

	resetPool() {
		this.currentPool = (<any>Object).assign([], this.symbolPool);
	}
}
