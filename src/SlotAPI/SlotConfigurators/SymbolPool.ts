import { SlotSymbol } from './SlotSymbol';

export interface SymbolPool {
	drawNextSymbol(): SlotSymbol;
	resetPool();
}
