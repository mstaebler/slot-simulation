"use strict";
// --------------------------
// Shared classes, enums and constants
var _a, _b, _c;
exports.__esModule = true;
exports.NUMBER_OF_ROWS = 3;
exports.NUMBER_OF_COLUMNS = 5;
exports.FRAGMENTS_NEEDED_FOR_KEY = 100;
var MySlotSymbol;
(function (MySlotSymbol) {
    MySlotSymbol["K1"] = "K1";
    MySlotSymbol["K2"] = "K2";
    MySlotSymbol["K3"] = "K3";
    MySlotSymbol["K4"] = "K4";
    MySlotSymbol["Ace"] = "Ace";
    MySlotSymbol["King"] = "King";
    MySlotSymbol["Queen"] = "Queen";
    MySlotSymbol["Jack"] = "Jack";
    MySlotSymbol["Ten"] = "Ten";
    MySlotSymbol["Weltreise"] = "Weltreise";
    MySlotSymbol["Scatter"] = "Scatter";
    MySlotSymbol["Wild"] = "Wild";
})(MySlotSymbol = exports.MySlotSymbol || (exports.MySlotSymbol = {}));
exports.SLOT_SYMBOL_MAP = (_a = {},
    _a[MySlotSymbol.K1] = { name: 'K1' },
    _a[MySlotSymbol.K2] = { name: 'K2' },
    _a[MySlotSymbol.K3] = { name: 'K3' },
    _a[MySlotSymbol.K4] = { name: 'K4' },
    _a[MySlotSymbol.Ace] = { name: 'Ace' },
    _a[MySlotSymbol.King] = { name: 'King' },
    _a[MySlotSymbol.Queen] = { name: 'Queen' },
    _a[MySlotSymbol.Jack] = { name: 'Jack' },
    _a[MySlotSymbol.Ten] = { name: 'Ten' },
    _a[MySlotSymbol.Weltreise] = { name: 'Weltreise' },
    _a[MySlotSymbol.Scatter] = { name: 'Scatter' },
    _a[MySlotSymbol.Wild] = { name: 'Wild' },
    _a);
exports.MULTIPLIER_MAP = (_b = {},
    _b[MySlotSymbol.K1] = [0, 0, 8, 100, 200],
    _b[MySlotSymbol.K2] = [0, 0, 5, 60, 150],
    _b[MySlotSymbol.K3] = [0, 0, 4, 50, 100],
    _b[MySlotSymbol.K4] = [0, 0, 3, 15, 40],
    _b[MySlotSymbol.Ace] = [0, 0, 2, 6, 10],
    _b[MySlotSymbol.King] = [0, 0, 2, 6, 10],
    _b[MySlotSymbol.Queen] = [0, 0, 1, 2, 6],
    _b[MySlotSymbol.Jack] = [0, 0, 1, 2, 6],
    _b[MySlotSymbol.Ten] = [0, 0, 0.5, 1, 4],
    _b[MySlotSymbol.Weltreise] = [0, 0, 0, 0, 0],
    _b[MySlotSymbol.Scatter] = [0, 0, 0, 0, 0],
    _b[MySlotSymbol.Wild] = [0, 0, 0, 0, 1000],
    _b); //
exports.BONUS_GAME_MAP = {
    0: 0,
    1: 0,
    2: 0,
    3: 2,
    4: 4,
    5: 7
};
exports.FREEGAMES_MAP = {
    0: 0,
    1: 0,
    2: 0,
    3: 10,
    4: 15,
    5: 20
};
exports.MY_PAY_LINES = [
    {
        reelCoordinates: [
            { reel: 1, row: 1 },
            { reel: 2, row: 1 },
            { reel: 3, row: 1 },
            { reel: 4, row: 1 },
            { reel: 5, row: 1 },
        ]
    },
    {
        reelCoordinates: [
            { reel: 1, row: 1 },
            { reel: 2, row: 2 },
            { reel: 3, row: 3 },
            { reel: 4, row: 2 },
            { reel: 5, row: 1 },
        ]
    },
    {
        reelCoordinates: [
            { reel: 1, row: 1 },
            { reel: 2, row: 1 },
            { reel: 3, row: 2 },
            { reel: 4, row: 3 },
            { reel: 5, row: 3 },
        ]
    },
    {
        reelCoordinates: [
            { reel: 1, row: 2 },
            { reel: 2, row: 2 },
            { reel: 3, row: 2 },
            { reel: 4, row: 2 },
            { reel: 5, row: 2 },
        ]
    },
    {
        reelCoordinates: [
            { reel: 1, row: 2 },
            { reel: 2, row: 2 },
            { reel: 3, row: 1 },
            { reel: 4, row: 2 },
            { reel: 5, row: 2 },
        ]
    },
    {
        reelCoordinates: [
            { reel: 1, row: 2 },
            { reel: 2, row: 2 },
            { reel: 3, row: 3 },
            { reel: 4, row: 2 },
            { reel: 5, row: 2 },
        ]
    },
    {
        reelCoordinates: [
            { reel: 1, row: 3 },
            { reel: 2, row: 3 },
            { reel: 3, row: 3 },
            { reel: 4, row: 3 },
            { reel: 5, row: 3 },
        ]
    },
    {
        reelCoordinates: [
            { reel: 1, row: 3 },
            { reel: 2, row: 2 },
            { reel: 3, row: 1 },
            { reel: 4, row: 2 },
            { reel: 5, row: 3 },
        ]
    },
    {
        reelCoordinates: [
            { reel: 1, row: 3 },
            { reel: 2, row: 3 },
            { reel: 3, row: 2 },
            { reel: 4, row: 1 },
            { reel: 5, row: 1 },
        ]
    },
];
var ChestType;
(function (ChestType) {
    ChestType[ChestType["Normal"] = 0] = "Normal";
    ChestType[ChestType["Golden"] = 1] = "Golden";
    ChestType[ChestType["Giant"] = 2] = "Giant";
})(ChestType = exports.ChestType || (exports.ChestType = {}));
var WORLD_TOUR_PLACE;
(function (WORLD_TOUR_PLACE) {
    WORLD_TOUR_PLACE[WORLD_TOUR_PLACE["Caesars Palace"] = 0] = "Caesars Palace";
    WORLD_TOUR_PLACE[WORLD_TOUR_PLACE["Borgata Hotel Casino"] = 1] = "Borgata Hotel Casino";
    WORLD_TOUR_PLACE[WORLD_TOUR_PLACE["Tigre de Cristal"] = 2] = "Tigre de Cristal";
    WORLD_TOUR_PLACE[WORLD_TOUR_PLACE["Tusk Rio Casino Resort"] = 3] = "Tusk Rio Casino Resort";
    WORLD_TOUR_PLACE[WORLD_TOUR_PLACE["Deltin Royale Casino"] = 4] = "Deltin Royale Casino";
    WORLD_TOUR_PLACE[WORLD_TOUR_PLACE["Atlantis Paradise Island"] = 5] = "Atlantis Paradise Island";
    WORLD_TOUR_PLACE[WORLD_TOUR_PLACE["Casino Lisboa"] = 6] = "Casino Lisboa";
    WORLD_TOUR_PLACE[WORLD_TOUR_PLACE["The Venetian Macao"] = 7] = "The Venetian Macao";
    WORLD_TOUR_PLACE[WORLD_TOUR_PLACE["Casino de Monte Carlo"] = 8] = "Casino de Monte Carlo";
    WORLD_TOUR_PLACE[WORLD_TOUR_PLACE["Twin Lions Casino"] = 9] = "Twin Lions Casino";
    WORLD_TOUR_PLACE[WORLD_TOUR_PLACE["Casino Sochi"] = 10] = "Casino Sochi";
    WORLD_TOUR_PLACE[WORLD_TOUR_PLACE["Crown Melbourne"] = 11] = "Crown Melbourne";
    WORLD_TOUR_PLACE[WORLD_TOUR_PLACE["Christchurch Casino"] = 12] = "Christchurch Casino";
    WORLD_TOUR_PLACE[WORLD_TOUR_PLACE["Trilenium Casino"] = 13] = "Trilenium Casino";
    WORLD_TOUR_PLACE[WORLD_TOUR_PLACE["Casino de Montr\u00E9al"] = 14] = "Casino de Montr\u00E9al";
})(WORLD_TOUR_PLACE = exports.WORLD_TOUR_PLACE || (exports.WORLD_TOUR_PLACE = {}));
exports.worldTourCoords = (_c = {},
    _c[WORLD_TOUR_PLACE["Caesars Palace"]] = { x: 130, y: 150 },
    _c[WORLD_TOUR_PLACE["Borgata Hotel Casino"]] = { x: 212, y: 147 },
    _c[WORLD_TOUR_PLACE["Tigre de Cristal"]] = { x: 630, y: 130 },
    _c[WORLD_TOUR_PLACE["Tusk Rio Casino Resort"]] = { x: 420, y: 270 },
    _c[WORLD_TOUR_PLACE["Deltin Royale Casino"]] = { x: 517, y: 200 },
    _c[WORLD_TOUR_PLACE["Atlantis Paradise Island"]] = { x: 206, y: 173 },
    _c[WORLD_TOUR_PLACE["Casino Lisboa"]] = { x: 345, y: 147 },
    _c[WORLD_TOUR_PLACE["The Venetian Macao"]] = { x: 605, y: 180 },
    _c[WORLD_TOUR_PLACE["Casino de Monte Carlo"]] = { x: 375, y: 140 },
    _c[WORLD_TOUR_PLACE["Twin Lions Casino"]] = { x: 145, y: 183 },
    _c[WORLD_TOUR_PLACE["Casino Sochi"]] = { x: 442, y: 140 },
    _c[WORLD_TOUR_PLACE["Crown Melbourne"]] = { x: 660, y: 300 },
    _c[WORLD_TOUR_PLACE["Christchurch Casino"]] = { x: 717, y: 320 },
    _c[WORLD_TOUR_PLACE["Trilenium Casino"]] = { x: 250, y: 280 },
    _c[WORLD_TOUR_PLACE["Casino de Montréal"]] = { x: 230, y: 120 },
    _c);
exports.worldTourCoordsToPlace = {
    0: WORLD_TOUR_PLACE["Caesars Palace"],
    1: WORLD_TOUR_PLACE["Borgata Hotel Casino"],
    2: WORLD_TOUR_PLACE["Tigre de Cristal"],
    3: WORLD_TOUR_PLACE["Tusk Rio Casino Resort"],
    4: WORLD_TOUR_PLACE["Deltin Royale Casino"],
    5: WORLD_TOUR_PLACE["Atlantis Paradise Island"],
    6: WORLD_TOUR_PLACE["Casino Lisboa"],
    7: WORLD_TOUR_PLACE["The Venetian Macao"],
    8: WORLD_TOUR_PLACE["Casino de Monte Carlo"],
    9: WORLD_TOUR_PLACE["Twin Lions Casino"],
    10: WORLD_TOUR_PLACE["Casino Sochi"],
    11: WORLD_TOUR_PLACE["Crown Melbourne"],
    12: WORLD_TOUR_PLACE["Christchurch Casino"],
    13: WORLD_TOUR_PLACE["Trilenium Casino"],
    14: WORLD_TOUR_PLACE["Casino de Montréal"]
};
// -----------------
// Server --> Client
exports.C_SLOT_RESULT = 'slotResult';
exports.C_SLOT_STATE = 'slotState';
exports.C_FREE_GAME_STATE = 'slotFreeGameState';
exports.C_BONUS_GAME_STATE = 'slotBonusGameState';
exports.C_FLY_TO_DESTINATION = 'flyToDestination';
exports.C_FREEGAME_SYMBOL_DRAW_RESULT = 'freegameSymbolDrawResult';
exports.C_WELTREISE_UPDATE = 'weltreiseUpdate';
// ------------------
// Client --> Server
exports.C_SLOT_UI_IS_READY = 'slotUiReady';
exports.C_SPIN_BUTTON_CLICK = 'spinButtonClick';
exports.C_DRAW_FREEGAME_SYMBOL = 'drawFreeGameSymbol';
exports.C_SWITCH_TO_PAYOUT = 'switchToPayout';
exports.C_SWITCH_TO_STOPPED = 'switchToStopped';
// -------------------------------------
function numberFormatWithDecimal(numberRaw, decimals) {
    if (numberRaw < 0) {
        var other = Math.floor(-numberRaw * Math.pow(10, decimals)) / Math.pow(10, decimals);
        if (other == 0) {
            return '0';
        }
        else {
            return '-' + numberFormatPos(other, decimals);
        }
    }
    else {
        return numberFormatPos(numberRaw, decimals);
    }
}
exports.numberFormatWithDecimal = numberFormatWithDecimal;
function numberFormatPos(numberRaw, decimals) {
    var number = '' + Math.floor(numberRaw * Math.pow(10, decimals)) / Math.pow(10, decimals);
    var minLength = 3;
    var array = number.split('.');
    if (array.length > 1) {
        minLength += array[1].length + 1;
    }
    number = number.replace('.', ',');
    if (number.length > minLength) {
        var mod = number.length % minLength;
        var output = mod > 0 ? number.substring(0, mod) : '';
        for (var i = 0; i < Math.floor(number.length / minLength); i++) {
            if (mod == 0 && i == 0)
                output += number.substring(mod + minLength * i, mod + minLength * i + minLength);
            else
                output += '.' + number.substring(mod + minLength * i, mod + minLength * i + minLength);
        }
        return output;
    }
    else {
        return number;
    }
}
