"use strict";
exports.__esModule = true;
var Simulator_1 = require("./Simulation/Simulator");
var Main = /** @class */ (function () {
    function Main() {
    }
    Main.main = function () {
        var sim = new Simulator_1.Simulator(1000000000, 1, 10000000, false, 10, false, false);
        sim.simulateSpin();
    };
    return Main;
}());
Main.main();
