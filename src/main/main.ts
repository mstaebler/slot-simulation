import {Simulator} from "./Simulation/Simulator";


class Main {
	public static main() {
		let sim = new Simulator(1000000000,1,10000000, false,10, false, false);
		sim.simulateSpin();
	}
}

Main.main();
