"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var SimpleFreegameSlotMachine_1 = require("../../SlotAPI/SlotMachine/SimpleFreegameSlotMachine");
var MySlotMachine = /** @class */ (function (_super) {
    __extends(MySlotMachine, _super);
    function MySlotMachine(startConfiguration, freegameSymbolProbability, symbolNameToSymbolMap) {
        return _super.call(this, startConfiguration, freegameSymbolProbability, symbolNameToSymbolMap) || this;
    }
    MySlotMachine.prototype.spin = function () {
        return this.getCurrentPicture().map(function (r) { return r.getSpinResult(); });
    };
    return MySlotMachine;
}(SimpleFreegameSlotMachine_1.SimpleFreegameSlotMachine));
exports.MySlotMachine = MySlotMachine;
