"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var SimpleSymbolPool_1 = require("../../SlotAPI/SlotConfigurators/SimpleSymbolPool");
var MySymbolPool = /** @class */ (function (_super) {
    __extends(MySymbolPool, _super);
    function MySymbolPool(symbolPool) {
        var _this = _super.call(this, symbolPool) || this;
        _this.currentPool = Object.assign([], symbolPool);
        return _this;
    }
    MySymbolPool.prototype.drawNextSymbol = function () {
        var index = Math.floor(Math.random() * this.currentPool.length);
        var returnSymbol = this.currentPool[index];
        var newPool = this.currentPool.filter(function (symbol, i) { return i != index; });
        this.currentPool = newPool;
        return returnSymbol;
    };
    MySymbolPool.prototype.addSymbol = function (symbol) {
        this.currentPool.push(symbol);
    };
    return MySymbolPool;
}(SimpleSymbolPool_1.SimpleSymbolPool));
exports.MySymbolPool = MySymbolPool;
