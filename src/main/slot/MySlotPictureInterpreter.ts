import {SimpleFreegameSlotPictureInterpreter} from "../../SlotAPI/SlotPictureInterpreter/SimpleFreegameSlotPictureInterpreter";
import {SlotSymbol} from "../../SlotAPI/SlotConfigurators/SlotSymbol";
import {PayLine} from "../../SlotAPI/SlotPictureInterpreter/PayLine";
import {LineResult} from "../../SlotAPI/SlotPictureInterpreter/LineResult";
import {MySlotResult} from "./MySlotResult";
import {SlotWin} from "../../SlotAPI/SlotPictureInterpreter/SlotWin";
import {SlotReel} from "../../SlotAPI/SlotMachine/SlotReel";

export class MySlotPictureInterpreter extends SimpleFreegameSlotPictureInterpreter {
	constructor(
		payLines: PayLine[],
		multiplierMap: { [key in string]: number[] },
		scatterSymbol: SlotSymbol,
		freeGamesMap: { [key in number]: number },
		private wildSymbol: SlotSymbol,
		private bonusGameSymbol: SlotSymbol,
		private bonusGamesMap: { [key in number]: number },
		private keyFragmentProbability: { [key in number]: number }
	) {
		super(payLines, multiplierMap, scatterSymbol, freeGamesMap);
	}

	interpretResult(spinResult: SlotReel[], buyIn: number): MySlotResult {
		let counter: number = 0;
		let result: MySlotResult = {
			bonusGamesWon: 0,
			fragmentMatrix: [],
			freeGamePayout: 0,
			freeGamesWon: 0,
			amountOfFragments: 0,
			payout: 0,
			slotPicture: spinResult.map(r => r.getSymbols()),
			wins: [],
		};
		this.getNormalWins(spinResult, result, buyIn, counter);
		result.bonusGamesWon = this.checkForBonusGamesWin(result.wins);
		result.freeGamesWon = this.checkForFreegamesWin(spinResult);
		result.amountOfFragments = this.getAmountOfKeyFragmentsToDisplay();
		if (result.amountOfFragments > 0) {
			result.fragmentMatrix = this.determineFragmentMatrix(result.amountOfFragments, spinResult);
		}
		return result;
	}

	public interpretResultAsFreeGameSpin(
		spinResult: SlotReel[],
		buyIn: number,
		freeGameSymbol: SlotSymbol
	): MySlotResult {
		let result: MySlotResult = this.interpretResult(spinResult, buyIn);

		result.freeGamePayout = this.calculateFreegamePayout(spinResult, freeGameSymbol, buyIn);
		return result;
	}

	private getNormalWins(spinResult: SlotReel[], result: MySlotResult, buyIn: number, counter: number) {
		this.payLines.forEach(line => {
			let lineSymbol: SlotSymbol = this.getSymbolByCoords(
				spinResult,
				line.reelCoordinates[0].reel,
				line.reelCoordinates[0].row
			);
			let lineResult: LineResult = this.getMatches(line, spinResult, lineSymbol, 1);
			let multiplier: number = this.multiplierMap[lineResult.symbol.name][lineResult.matches - 1];
			if (
				multiplier > 0 ||
				(lineResult.symbol.name == this.bonusGameSymbol.name &&
					this.bonusGamesMap[lineResult.matches] > 0)
			) {
				result.wins.push({
					amount: multiplier * buyIn,
					symbol: lineResult.symbol,
					winningLine: counter,
					matches: lineResult.matches,
				});
				result.payout += multiplier * buyIn;
			}
			counter++;
		});
	}

	private calculateFreegamePayout(spinResult: SlotReel[], freegameSymbol: SlotSymbol, buyIn: number): number {
		let columnsHit: number = 0;
		spinResult.forEach(reel => {
			let hit = false;
			reel.getSymbols().forEach(symbol => {
				if (symbol.name == freegameSymbol.name) {
					hit = true;
				}
			});
			if (hit) {
				columnsHit++;
			}
		});
		if (columnsHit > 0) {
			return this.payLines.length * this.multiplierMap[freegameSymbol.name][columnsHit - 1] * buyIn;
		}
		return 0;
	}

	private checkForFreegamesWin(spinResult: SlotReel[]) {
		return this.freeGamesMap[this.countScatter(spinResult)];
	}

	private countScatter(spinResult: SlotReel[]) {
		let amountOfScatter: number = 0;
		spinResult.forEach(reel => {
			reel.getSymbols().forEach(slotSymbol => {
				if (slotSymbol.name == this.scatterSymbol.name) {
					amountOfScatter++;
				}
			});
		});
		return amountOfScatter;
	}

	private getAmountOfKeyFragmentsToDisplay(): number {
		let randomThreshold = Math.random() * 100;
		let accumulatedThreshold = 0;
		for (let key in this.keyFragmentProbability) {
			accumulatedThreshold += this.keyFragmentProbability[key];
			if (randomThreshold < accumulatedThreshold) {
				return Number(key);
			}
		}
	}

	private getSymbolByCoords(spinResult: SlotReel[], column: number, row: number): SlotSymbol {
		return spinResult[column - 1].getSymbols()[row - 1];
	}

	private getMatches(line: PayLine, spinResult: SlotReel[], symbol: SlotSymbol, matches: number): LineResult {
		if (matches >= spinResult.length) {
			return {
				symbol: symbol,
				matches: matches,
			};
		}

		let currentSymbol: SlotSymbol = this.getSymbolByCoords(
			spinResult,
			line.reelCoordinates[matches].reel,
			line.reelCoordinates[matches].row
		);

		if (symbol.name == this.wildSymbol.name) {
			if (currentSymbol.name == this.wildSymbol.name) {
				return this.getMatches(line, spinResult, symbol, ++matches);
			} else {
				return this.getMatches(line, spinResult, currentSymbol, ++matches);
			}
		} else {
			if (
				currentSymbol.name == symbol.name ||
				currentSymbol.name == this.wildSymbol.name
			) {
				return this.getMatches(line, spinResult, symbol, ++matches);
			} else {
				return {
					matches: matches,
					symbol: symbol,
				};
			}
		}
	}

	private checkForBonusGamesWin(wins: SlotWin[]) {
		let wonBonusGames: number = 0;
		wins.forEach(w => {
			if (w.symbol.name == this.bonusGameSymbol.name) {
				wonBonusGames += this.bonusGamesMap[w.matches];
			}
		});
		return wonBonusGames;
	}

	private determineFragmentMatrix(amountOfFragments: number, spinResult: SlotReel[]): boolean[][] {
		let matrix: boolean[][] = [];
		for (let reelIndex = 0; reelIndex < spinResult.length; reelIndex++) {
			let reel: boolean[] = [];
			for (let row = 0; row < spinResult[reelIndex].getSymbols().length; row++) {
				reel.push(false);
			}
			matrix.push(reel);
		}

		let i = amountOfFragments;
		while (i > 0) {
			let column: number = Math.floor(Math.random() * spinResult.length);
			let row: number = Math.floor(Math.random() * spinResult[column].getSymbols().length);
			if (!matrix[column][row]) {
				matrix[column][row] = true;
				i--;
			}
		}

		return matrix;
	}
}
