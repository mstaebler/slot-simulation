"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var SimpleSlotReel_1 = require("../../SlotAPI/SlotMachine/SimpleSlotReel");
var MySlotReel = /** @class */ (function (_super) {
    __extends(MySlotReel, _super);
    function MySlotReel(symbols, symbolPool) {
        return _super.call(this, symbols, symbolPool) || this;
    }
    MySlotReel.prototype.addSymbol = function (symbol) {
        var mySymbolPool = this.getSymbolPool();
        mySymbolPool.addSymbol(symbol);
    };
    return MySlotReel;
}(SimpleSlotReel_1.SimpleSlotReel));
exports.MySlotReel = MySlotReel;
