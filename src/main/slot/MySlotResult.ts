import {SlotResult} from "../../SlotAPI/SlotPictureInterpreter/SlotResult";

export interface MySlotResult extends SlotResult {
	amountOfFragments: number;
	freeGamesWon: number;
	bonusGamesWon: number;
	freeGamePayout: number;
	fragmentMatrix: boolean[][];
}
