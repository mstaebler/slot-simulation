"use strict";
var _a;
exports.__esModule = true;
var protocol_1 = require("../protocol");
exports.FIRST_REEL_SYMBOL_POOL = [
    { name: protocol_1.MySlotSymbol.K1 },
    { name: protocol_1.MySlotSymbol.K2 },
    { name: protocol_1.MySlotSymbol.K2 },
    { name: protocol_1.MySlotSymbol.K3 },
    { name: protocol_1.MySlotSymbol.K3 },
    { name: protocol_1.MySlotSymbol.K4 },
    { name: protocol_1.MySlotSymbol.K4 },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Scatter },
    { name: protocol_1.MySlotSymbol.Wild },
    { name: protocol_1.MySlotSymbol.Weltreise },
    { name: protocol_1.MySlotSymbol.Weltreise },
    { name: protocol_1.MySlotSymbol.Weltreise },
];
exports.SECOND_REEL_SYMBOL_POOL = [
    { name: protocol_1.MySlotSymbol.K1 },
    { name: protocol_1.MySlotSymbol.K1 },
    { name: protocol_1.MySlotSymbol.K2 },
    { name: protocol_1.MySlotSymbol.K3 },
    { name: protocol_1.MySlotSymbol.K3 },
    { name: protocol_1.MySlotSymbol.K4 },
    { name: protocol_1.MySlotSymbol.K4 },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Scatter },
    { name: protocol_1.MySlotSymbol.Wild },
    { name: protocol_1.MySlotSymbol.Weltreise },
    { name: protocol_1.MySlotSymbol.Weltreise },
];
exports.THIRD_REEL_SYMBOL_POOL = [
    { name: protocol_1.MySlotSymbol.K1 },
    { name: protocol_1.MySlotSymbol.K2 },
    { name: protocol_1.MySlotSymbol.K2 },
    { name: protocol_1.MySlotSymbol.K3 },
    { name: protocol_1.MySlotSymbol.K4 },
    { name: protocol_1.MySlotSymbol.K4 },
    { name: protocol_1.MySlotSymbol.K4 },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Scatter },
    { name: protocol_1.MySlotSymbol.Wild },
    { name: protocol_1.MySlotSymbol.Weltreise },
    { name: protocol_1.MySlotSymbol.Weltreise },
];
exports.FOURTH_REEL_SYMBOL_POOL = [
    { name: protocol_1.MySlotSymbol.K1 },
    { name: protocol_1.MySlotSymbol.K2 },
    { name: protocol_1.MySlotSymbol.K3 },
    { name: protocol_1.MySlotSymbol.K3 },
    { name: protocol_1.MySlotSymbol.K4 },
    { name: protocol_1.MySlotSymbol.K4 },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Scatter },
    { name: protocol_1.MySlotSymbol.Wild },
    { name: protocol_1.MySlotSymbol.Weltreise },
    { name: protocol_1.MySlotSymbol.Weltreise },
    { name: protocol_1.MySlotSymbol.Weltreise },
];
exports.FIFTH_REEL_SYMBOL_POOL = [
    { name: protocol_1.MySlotSymbol.K1 },
    { name: protocol_1.MySlotSymbol.K2 },
    { name: protocol_1.MySlotSymbol.K2 },
    { name: protocol_1.MySlotSymbol.K3 },
    { name: protocol_1.MySlotSymbol.K3 },
    { name: protocol_1.MySlotSymbol.K4 },
    { name: protocol_1.MySlotSymbol.K4 },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.Ace },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.King },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Queen },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Jack },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Ten },
    { name: protocol_1.MySlotSymbol.Scatter },
    { name: protocol_1.MySlotSymbol.Wild },
    { name: protocol_1.MySlotSymbol.Weltreise },
    { name: protocol_1.MySlotSymbol.Weltreise },
];
exports.REEL_SYMBOL_POOLS = [
    exports.FIRST_REEL_SYMBOL_POOL,
    exports.SECOND_REEL_SYMBOL_POOL,
    exports.THIRD_REEL_SYMBOL_POOL,
    exports.FOURTH_REEL_SYMBOL_POOL,
    exports.FIFTH_REEL_SYMBOL_POOL,
];
exports.START_CONFIGURATION = [
    { name: protocol_1.MySlotSymbol.K1 },
    { name: protocol_1.MySlotSymbol.K1 },
    { name: protocol_1.MySlotSymbol.K1 },
];
exports.FreegameSymbolProbability = (_a = {},
    _a[protocol_1.MySlotSymbol.K1] = 3,
    _a[protocol_1.MySlotSymbol.K2] = 5,
    _a[protocol_1.MySlotSymbol.K3] = 7,
    _a[protocol_1.MySlotSymbol.K4] = 9,
    _a[protocol_1.MySlotSymbol.Ace] = 15,
    _a[protocol_1.MySlotSymbol.King] = 15,
    _a[protocol_1.MySlotSymbol.Queen] = 16,
    _a[protocol_1.MySlotSymbol.Jack] = 16,
    _a[protocol_1.MySlotSymbol.Ten] = 14,
    _a[protocol_1.MySlotSymbol.Wild] = 0,
    _a[protocol_1.MySlotSymbol.Scatter] = 0,
    _a[protocol_1.MySlotSymbol.Weltreise] = 0,
    _a);
exports.KEY_FRAGMENT_PROBABILITY = {
    0: 54.3,
    1: 36.7,
    2: 5,
    3: 2.5,
    4: 1,
    5: 0.5,
    6: 0,
    7: 0
};
exports.indexOfReelToAddSymbolInFreegames = {
    Ten: [],
    Jack: [],
    Queen: [],
    King: [2],
    Ace: [2],
    K4: [0, 1, 3],
    K3: [1, 2, 3, 4],
    K2: [1, 2, 3],
    K1: [0, 2, 3, 4]
};
