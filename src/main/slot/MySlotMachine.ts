import {SimpleFreegameSlotMachine} from "../../SlotAPI/SlotMachine/SimpleFreegameSlotMachine";
import {MySlotReel} from "./MySlotReel";
import {SlotSymbol} from "../../SlotAPI/SlotConfigurators/SlotSymbol";
import {SlotReel} from "../../SlotAPI/SlotMachine/SlotReel";

export class MySlotMachine extends SimpleFreegameSlotMachine {
	constructor(
		startConfiguration: MySlotReel[],
		freegameSymbolProbability: { [key in string]: number },
		symbolNameToSymbolMap: { [key in string]: SlotSymbol }
	) {
		super(startConfiguration, freegameSymbolProbability, symbolNameToSymbolMap);
	}

	spin(): SlotReel[] {
		return this.getCurrentPicture().map(r => r.getSpinResult());
	}
}
