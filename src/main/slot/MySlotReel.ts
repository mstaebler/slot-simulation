import {SimpleSlotReel} from "../../SlotAPI/SlotMachine/SimpleSlotReel";
import {SlotSymbol} from "../../SlotAPI/SlotConfigurators/SlotSymbol";
import {SymbolPool} from "../../SlotAPI/SlotConfigurators/SymbolPool";
import {MySymbolPool} from "./MySymbolPool";

export class MySlotReel extends SimpleSlotReel {
	constructor(symbols: SlotSymbol[], symbolPool: SymbolPool) {
		super(symbols, symbolPool);
	}

	addSymbol(symbol: SlotSymbol) {
		let mySymbolPool: MySymbolPool = this.getSymbolPool() as MySymbolPool;
		mySymbolPool.addSymbol(symbol);
	}
}
