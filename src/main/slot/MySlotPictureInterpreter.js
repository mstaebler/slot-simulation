"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var SimpleFreegameSlotPictureInterpreter_1 = require("../../SlotAPI/SlotPictureInterpreter/SimpleFreegameSlotPictureInterpreter");
var MySlotPictureInterpreter = /** @class */ (function (_super) {
    __extends(MySlotPictureInterpreter, _super);
    function MySlotPictureInterpreter(payLines, multiplierMap, scatterSymbol, freeGamesMap, wildSymbol, bonusGameSymbol, bonusGamesMap, keyFragmentProbability) {
        var _this = _super.call(this, payLines, multiplierMap, scatterSymbol, freeGamesMap) || this;
        _this.wildSymbol = wildSymbol;
        _this.bonusGameSymbol = bonusGameSymbol;
        _this.bonusGamesMap = bonusGamesMap;
        _this.keyFragmentProbability = keyFragmentProbability;
        return _this;
    }
    MySlotPictureInterpreter.prototype.interpretResult = function (spinResult, buyIn) {
        var counter = 0;
        var result = {
            bonusGamesWon: 0,
            fragmentMatrix: [],
            freeGamePayout: 0,
            freeGamesWon: 0,
            amountOfFragments: 0,
            payout: 0,
            slotPicture: spinResult.map(function (r) { return r.getSymbols(); }),
            wins: []
        };
        this.getNormalWins(spinResult, result, buyIn, counter);
        result.bonusGamesWon = this.checkForBonusGamesWin(result.wins);
        result.freeGamesWon = this.checkForFreegamesWin(spinResult);
        result.amountOfFragments = this.getAmountOfKeyFragmentsToDisplay();
        if (result.amountOfFragments > 0) {
            result.fragmentMatrix = this.determineFragmentMatrix(result.amountOfFragments, spinResult);
        }
        return result;
    };
    MySlotPictureInterpreter.prototype.interpretResultAsFreeGameSpin = function (spinResult, buyIn, freeGameSymbol) {
        var result = this.interpretResult(spinResult, buyIn);
        result.freeGamePayout = this.calculateFreegamePayout(spinResult, freeGameSymbol, buyIn);
        return result;
    };
    MySlotPictureInterpreter.prototype.getNormalWins = function (spinResult, result, buyIn, counter) {
        var _this = this;
        this.payLines.forEach(function (line) {
            var lineSymbol = _this.getSymbolByCoords(spinResult, line.reelCoordinates[0].reel, line.reelCoordinates[0].row);
            var lineResult = _this.getMatches(line, spinResult, lineSymbol, 1);
            var multiplier = _this.multiplierMap[lineResult.symbol.name][lineResult.matches - 1];
            if (multiplier > 0 ||
                (lineResult.symbol.name == _this.bonusGameSymbol.name &&
                    _this.bonusGamesMap[lineResult.matches] > 0)) {
                result.wins.push({
                    amount: multiplier * buyIn,
                    symbol: lineResult.symbol,
                    winningLine: counter,
                    matches: lineResult.matches
                });
                result.payout += multiplier * buyIn;
            }
            counter++;
        });
    };
    MySlotPictureInterpreter.prototype.calculateFreegamePayout = function (spinResult, freegameSymbol, buyIn) {
        var columnsHit = 0;
        spinResult.forEach(function (reel) {
            var hit = false;
            reel.getSymbols().forEach(function (symbol) {
                if (symbol.name == freegameSymbol.name) {
                    hit = true;
                }
            });
            if (hit) {
                columnsHit++;
            }
        });
        if (columnsHit > 0) {
            return this.payLines.length * this.multiplierMap[freegameSymbol.name][columnsHit - 1] * buyIn;
        }
        return 0;
    };
    MySlotPictureInterpreter.prototype.checkForFreegamesWin = function (spinResult) {
        return this.freeGamesMap[this.countScatter(spinResult)];
    };
    MySlotPictureInterpreter.prototype.countScatter = function (spinResult) {
        var _this = this;
        var amountOfScatter = 0;
        spinResult.forEach(function (reel) {
            reel.getSymbols().forEach(function (slotSymbol) {
                if (slotSymbol.name == _this.scatterSymbol.name) {
                    amountOfScatter++;
                }
            });
        });
        return amountOfScatter;
    };
    MySlotPictureInterpreter.prototype.getAmountOfKeyFragmentsToDisplay = function () {
        var randomThreshold = Math.random() * 100;
        var accumulatedThreshold = 0;
        for (var key in this.keyFragmentProbability) {
            accumulatedThreshold += this.keyFragmentProbability[key];
            if (randomThreshold < accumulatedThreshold) {
                return Number(key);
            }
        }
    };
    MySlotPictureInterpreter.prototype.getSymbolByCoords = function (spinResult, column, row) {
        return spinResult[column - 1].getSymbols()[row - 1];
    };
    MySlotPictureInterpreter.prototype.getMatches = function (line, spinResult, symbol, matches) {
        if (matches >= spinResult.length) {
            return {
                symbol: symbol,
                matches: matches
            };
        }
        var currentSymbol = this.getSymbolByCoords(spinResult, line.reelCoordinates[matches].reel, line.reelCoordinates[matches].row);
        if (symbol.name == this.wildSymbol.name) {
            if (currentSymbol.name == this.wildSymbol.name) {
                return this.getMatches(line, spinResult, symbol, ++matches);
            }
            else {
                return this.getMatches(line, spinResult, currentSymbol, ++matches);
            }
        }
        else {
            if (currentSymbol.name == symbol.name ||
                currentSymbol.name == this.wildSymbol.name) {
                return this.getMatches(line, spinResult, symbol, ++matches);
            }
            else {
                return {
                    matches: matches,
                    symbol: symbol
                };
            }
        }
    };
    MySlotPictureInterpreter.prototype.checkForBonusGamesWin = function (wins) {
        var _this = this;
        var wonBonusGames = 0;
        wins.forEach(function (w) {
            if (w.symbol.name == _this.bonusGameSymbol.name) {
                wonBonusGames += _this.bonusGamesMap[w.matches];
            }
        });
        return wonBonusGames;
    };
    MySlotPictureInterpreter.prototype.determineFragmentMatrix = function (amountOfFragments, spinResult) {
        var matrix = [];
        for (var reelIndex = 0; reelIndex < spinResult.length; reelIndex++) {
            var reel = [];
            for (var row = 0; row < spinResult[reelIndex].getSymbols().length; row++) {
                reel.push(false);
            }
            matrix.push(reel);
        }
        var i = amountOfFragments;
        while (i > 0) {
            var column = Math.floor(Math.random() * spinResult.length);
            var row = Math.floor(Math.random() * spinResult[column].getSymbols().length);
            if (!matrix[column][row]) {
                matrix[column][row] = true;
                i--;
            }
        }
        return matrix;
    };
    return MySlotPictureInterpreter;
}(SimpleFreegameSlotPictureInterpreter_1.SimpleFreegameSlotPictureInterpreter));
exports.MySlotPictureInterpreter = MySlotPictureInterpreter;
