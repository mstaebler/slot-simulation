import {SimpleSymbolPool} from "../../SlotAPI/SlotConfigurators/SimpleSymbolPool";
import {SlotSymbol} from "../../SlotAPI/SlotConfigurators/SlotSymbol";

export class MySymbolPool extends SimpleSymbolPool {
	currentPool: SlotSymbol[];

	constructor(symbolPool: SlotSymbol[]) {
		super(symbolPool);
		this.currentPool = (<any>Object).assign([], symbolPool);
	}

	drawNextSymbol(): SlotSymbol {
		let index: number = Math.floor(Math.random() * this.currentPool.length);
		let returnSymbol: SlotSymbol = this.currentPool[index];
		let newPool: SlotSymbol[] = this.currentPool.filter((symbol, i) => i != index);
		this.currentPool = newPool;
		return returnSymbol;
	}

	addSymbol(symbol: SlotSymbol) {
		this.currentPool.push(symbol);
	}
}
