import {SlotSymbol} from "../../SlotAPI/SlotConfigurators/SlotSymbol";
import {MySlotSymbol} from "../protocol";

export const FIRST_REEL_SYMBOL_POOL: SlotSymbol[] = [
    { name: MySlotSymbol.K1 },

    { name: MySlotSymbol.K2 },
    { name: MySlotSymbol.K2 },

    { name: MySlotSymbol.K3 },
    { name: MySlotSymbol.K3 },

    { name: MySlotSymbol.K4 },
    { name: MySlotSymbol.K4 },

    { name: MySlotSymbol.Ace },
    { name: MySlotSymbol.Ace },
    { name: MySlotSymbol.Ace },

    { name: MySlotSymbol.King },
    { name: MySlotSymbol.King },
    { name: MySlotSymbol.King },

    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },

    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },

    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },

    { name: MySlotSymbol.Scatter },

    { name: MySlotSymbol.Wild },

    { name: MySlotSymbol.Weltreise },
    { name: MySlotSymbol.Weltreise },
    { name: MySlotSymbol.Weltreise },
];

export const SECOND_REEL_SYMBOL_POOL: SlotSymbol[] = [
    { name: MySlotSymbol.K1 },
    { name: MySlotSymbol.K1 },

    { name: MySlotSymbol.K2 },

    { name: MySlotSymbol.K3 },
    { name: MySlotSymbol.K3 },

    { name: MySlotSymbol.K4 },
    { name: MySlotSymbol.K4 },

    { name: MySlotSymbol.Ace },
    { name: MySlotSymbol.Ace },
    { name: MySlotSymbol.Ace },

    { name: MySlotSymbol.King },
    { name: MySlotSymbol.King },
    { name: MySlotSymbol.King },

    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },

    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },

    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },

    { name: MySlotSymbol.Scatter },

    { name: MySlotSymbol.Wild },

    { name: MySlotSymbol.Weltreise },
    { name: MySlotSymbol.Weltreise },
];

export const THIRD_REEL_SYMBOL_POOL: SlotSymbol[] = [
    { name: MySlotSymbol.K1 },

    { name: MySlotSymbol.K2 },
    { name: MySlotSymbol.K2 },

    { name: MySlotSymbol.K3 },

    { name: MySlotSymbol.K4 },
    { name: MySlotSymbol.K4 },
    { name: MySlotSymbol.K4 },

    { name: MySlotSymbol.Ace },
    { name: MySlotSymbol.Ace },
    { name: MySlotSymbol.Ace },

    { name: MySlotSymbol.King },
    { name: MySlotSymbol.King },
    { name: MySlotSymbol.King },

    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },

    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },

    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },

    { name: MySlotSymbol.Scatter },

    { name: MySlotSymbol.Wild },

    { name: MySlotSymbol.Weltreise },
    { name: MySlotSymbol.Weltreise },
];

export const FOURTH_REEL_SYMBOL_POOL: SlotSymbol[] = [
    { name: MySlotSymbol.K1 },

    { name: MySlotSymbol.K2 },

    { name: MySlotSymbol.K3 },
    { name: MySlotSymbol.K3 },

    { name: MySlotSymbol.K4 },
    { name: MySlotSymbol.K4 },

    { name: MySlotSymbol.Ace },
    { name: MySlotSymbol.Ace },
    { name: MySlotSymbol.Ace },

    { name: MySlotSymbol.King },
    { name: MySlotSymbol.King },
    { name: MySlotSymbol.King },

    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },

    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },

    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },

    { name: MySlotSymbol.Scatter },

    { name: MySlotSymbol.Wild },

    { name: MySlotSymbol.Weltreise },
    { name: MySlotSymbol.Weltreise },
    { name: MySlotSymbol.Weltreise },
];

export const FIFTH_REEL_SYMBOL_POOL: SlotSymbol[] = [
    { name: MySlotSymbol.K1 },

    { name: MySlotSymbol.K2 },
    { name: MySlotSymbol.K2 },

    { name: MySlotSymbol.K3 },
    { name: MySlotSymbol.K3 },

    { name: MySlotSymbol.K4 },
    { name: MySlotSymbol.K4 },

    { name: MySlotSymbol.Ace },
    { name: MySlotSymbol.Ace },
    { name: MySlotSymbol.Ace },
    { name: MySlotSymbol.Ace },

    { name: MySlotSymbol.King },
    { name: MySlotSymbol.King },
    { name: MySlotSymbol.King },
    { name: MySlotSymbol.King },

    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },
    { name: MySlotSymbol.Queen },

    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },
    { name: MySlotSymbol.Jack },

    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },
    { name: MySlotSymbol.Ten },

    { name: MySlotSymbol.Scatter },

    { name: MySlotSymbol.Wild },

    { name: MySlotSymbol.Weltreise },
    { name: MySlotSymbol.Weltreise },
];

export const REEL_SYMBOL_POOLS: SlotSymbol[][] = [
    FIRST_REEL_SYMBOL_POOL,
    SECOND_REEL_SYMBOL_POOL,
    THIRD_REEL_SYMBOL_POOL,
    FOURTH_REEL_SYMBOL_POOL,
    FIFTH_REEL_SYMBOL_POOL,
];

export const START_CONFIGURATION: SlotSymbol[] = [
    { name: MySlotSymbol.K1 },
    { name: MySlotSymbol.K1 },
    { name: MySlotSymbol.K1 },
];

export const FreegameSymbolProbability: { [key in MySlotSymbol]: number } = {
    [MySlotSymbol.K1]: 3,
    [MySlotSymbol.K2]: 5,
    [MySlotSymbol.K3]: 7,
    [MySlotSymbol.K4]: 9,
    [MySlotSymbol.Ace]: 15,
    [MySlotSymbol.King]: 15,
    [MySlotSymbol.Queen]: 16,
    [MySlotSymbol.Jack]: 16,
    [MySlotSymbol.Ten]: 14,
    [MySlotSymbol.Wild]: 0,
    [MySlotSymbol.Scatter]: 0,
    [MySlotSymbol.Weltreise]: 0,
};

export const KEY_FRAGMENT_PROBABILITY: { [key in number]: number } = {
    0: 54.3,
    1: 36.7,
    2: 5,
    3: 2.5,
    4: 1,
    5: 0.5,
    6: 0,
    7: 0,
};

export const indexOfReelToAddSymbolInFreegames: { [key in string]: number[] } = {
    Ten: [],
    Jack: [],
    Queen: [],
    King: [2],
    Ace: [2],
    K4: [0, 1, 3],
    K3: [1, 2, 3, 4],
    K2: [1, 2, 3],
    K1: [0, 2, 3, 4],
};
