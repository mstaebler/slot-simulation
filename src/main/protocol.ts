// --------------------------
// Shared classes, enums and constants

import {SlotSymbol} from "../SlotAPI/SlotConfigurators/SlotSymbol";
import {PayLine} from "../SlotAPI/SlotPictureInterpreter/PayLine";
import {MySlotResult} from "./slot/MySlotResult";

export const NUMBER_OF_ROWS: number = 3;
export const NUMBER_OF_COLUMNS: number = 5;
export const FRAGMENTS_NEEDED_FOR_KEY: number = 100;

export enum MySlotSymbol {
	K1 = 'K1',
	K2 = 'K2',
	K3 = 'K3',
	K4 = 'K4',
	Ace = 'Ace',
	King = 'King',
	Queen = 'Queen',
	Jack = 'Jack',
	Ten = 'Ten',
	Weltreise = 'Weltreise',
	Scatter = 'Scatter',
	Wild = 'Wild',
}

export const SLOT_SYMBOL_MAP: { [key in MySlotSymbol]: SlotSymbol } = {
	[MySlotSymbol.K1]: { name: 'K1' },
	[MySlotSymbol.K2]: { name: 'K2' },
	[MySlotSymbol.K3]: { name: 'K3' },
	[MySlotSymbol.K4]: { name: 'K4' },
	[MySlotSymbol.Ace]: { name: 'Ace' },
	[MySlotSymbol.King]: { name: 'King' },
	[MySlotSymbol.Queen]: { name: 'Queen' },
	[MySlotSymbol.Jack]: { name: 'Jack' },
	[MySlotSymbol.Ten]: { name: 'Ten' },
	[MySlotSymbol.Weltreise]: { name: 'Weltreise' },
	[MySlotSymbol.Scatter]: { name: 'Scatter' },
	[MySlotSymbol.Wild]: { name: 'Wild' },
};


export const MULTIPLIER_MAP: { [key in MySlotSymbol]: number[] } = {
	[MySlotSymbol.K1]: [0, 0, 8, 100, 200],
	[MySlotSymbol.K2]: [0, 0, 5, 60, 150],
	[MySlotSymbol.K3]: [0, 0, 4, 50, 100],
	[MySlotSymbol.K4]: [0, 0, 3, 15, 40],
	[MySlotSymbol.Ace]: [0, 0, 2, 6, 10],
	[MySlotSymbol.King]: [0, 0, 2, 6, 10],
	[MySlotSymbol.Queen]: [0, 0, 1, 2, 6],
	[MySlotSymbol.Jack]: [0, 0, 1, 2, 6],
	[MySlotSymbol.Ten]: [0, 0, 0.5, 1, 4],
	[MySlotSymbol.Weltreise]: [0, 0, 0, 0, 0],
	[MySlotSymbol.Scatter]: [0, 0, 0, 0, 0],
	[MySlotSymbol.Wild]: [0, 0, 0, 0, 1000],
}; //

export const BONUS_GAME_MAP: { [key in number]: number } = {
	0: 0,
	1: 0,
	2: 0,
	3: 2,
	4: 4,
	5: 7,
};

export const FREEGAMES_MAP: { [key in number]: number } = {
	0: 0,
	1: 0,
	2: 0,
	3: 10,
	4: 15,
	5: 20,
};

export const MY_PAY_LINES: PayLine[] = [
	{
		reelCoordinates: [
			{ reel: 1, row: 1 },
			{ reel: 2, row: 1 },
			{ reel: 3, row: 1 },
			{ reel: 4, row: 1 },
			{ reel: 5, row: 1 },
		],
	},
	{
		reelCoordinates: [
			{ reel: 1, row: 1 },
			{ reel: 2, row: 2 },
			{ reel: 3, row: 3 },
			{ reel: 4, row: 2 },
			{ reel: 5, row: 1 },
		],
	},
	{
		reelCoordinates: [
			{ reel: 1, row: 1 },
			{ reel: 2, row: 1 },
			{ reel: 3, row: 2 },
			{ reel: 4, row: 3 },
			{ reel: 5, row: 3 },
		],
	},
	{
		reelCoordinates: [
			{ reel: 1, row: 2 },
			{ reel: 2, row: 2 },
			{ reel: 3, row: 2 },
			{ reel: 4, row: 2 },
			{ reel: 5, row: 2 },
		],
	},

	{
		reelCoordinates: [
			{ reel: 1, row: 2 },
			{ reel: 2, row: 2 },
			{ reel: 3, row: 1 },
			{ reel: 4, row: 2 },
			{ reel: 5, row: 2 },
		],
	},
	{
		reelCoordinates: [
			{ reel: 1, row: 2 },
			{ reel: 2, row: 2 },
			{ reel: 3, row: 3 },
			{ reel: 4, row: 2 },
			{ reel: 5, row: 2 },
		],
	},
	{
		reelCoordinates: [
			{ reel: 1, row: 3 },
			{ reel: 2, row: 3 },
			{ reel: 3, row: 3 },
			{ reel: 4, row: 3 },
			{ reel: 5, row: 3 },
		],
	},
	{
		reelCoordinates: [
			{ reel: 1, row: 3 },
			{ reel: 2, row: 2 },
			{ reel: 3, row: 1 },
			{ reel: 4, row: 2 },
			{ reel: 5, row: 3 },
		],
	},
	{
		reelCoordinates: [
			{ reel: 1, row: 3 },
			{ reel: 2, row: 3 },
			{ reel: 3, row: 2 },
			{ reel: 4, row: 1 },
			{ reel: 5, row: 1 },
		],
	},
];

export enum ChestType {
	Normal,
	Golden,
	Giant,
}

export enum WORLD_TOUR_PLACE {
	"Caesars Palace",
	"Borgata Hotel Casino",
	"Tigre de Cristal",
	"Tusk Rio Casino Resort",
	"Deltin Royale Casino",
	"Atlantis Paradise Island",
	"Casino Lisboa",
	"The Venetian Macao",
	"Casino de Monte Carlo",
	"Twin Lions Casino",
	"Casino Sochi",
	"Crown Melbourne",
	"Christchurch Casino" ,
	"Trilenium Casino",
	"Casino de Montréal",
}


export interface Coords {
	x: number;
	y: number;
}

export const worldTourCoords: { [key in WORLD_TOUR_PLACE]: Coords } = {
	[WORLD_TOUR_PLACE["Caesars Palace"]]: { x: 130, y: 150 },
	[WORLD_TOUR_PLACE["Borgata Hotel Casino"]]: { x: 212, y: 147 },
	[WORLD_TOUR_PLACE["Tigre de Cristal"]]: { x: 630, y: 130 },
	[WORLD_TOUR_PLACE["Tusk Rio Casino Resort"]]: { x: 420, y: 270 },
	[WORLD_TOUR_PLACE["Deltin Royale Casino"]]: { x: 517, y: 200 },
	[WORLD_TOUR_PLACE["Atlantis Paradise Island"]]: { x: 206, y: 173 },
	[WORLD_TOUR_PLACE["Casino Lisboa"]]: { x: 345, y: 147 },
	[WORLD_TOUR_PLACE["The Venetian Macao"]]: { x: 605, y: 180 },
	[WORLD_TOUR_PLACE["Casino de Monte Carlo"]]: { x: 375, y: 140 },
	[WORLD_TOUR_PLACE["Twin Lions Casino"]]: { x: 145, y: 183 },
	[WORLD_TOUR_PLACE["Casino Sochi"]]: { x: 442, y: 140 },
	[WORLD_TOUR_PLACE["Crown Melbourne"]]: { x: 660, y: 300 },
	[WORLD_TOUR_PLACE["Christchurch Casino"]]: { x: 717, y: 320 },
	[WORLD_TOUR_PLACE["Trilenium Casino"]]: { x: 250, y: 280 },
	[WORLD_TOUR_PLACE["Casino de Montréal"]]: { x: 230, y: 120 },
};

export const worldTourCoordsToPlace: { [key in number]: WORLD_TOUR_PLACE } = {
	0:WORLD_TOUR_PLACE["Caesars Palace"],
	1:WORLD_TOUR_PLACE["Borgata Hotel Casino"],
	2:WORLD_TOUR_PLACE["Tigre de Cristal"],
	3:WORLD_TOUR_PLACE["Tusk Rio Casino Resort"],
	4:WORLD_TOUR_PLACE["Deltin Royale Casino"],
	5:WORLD_TOUR_PLACE["Atlantis Paradise Island"],
	6:WORLD_TOUR_PLACE["Casino Lisboa"],
	7:WORLD_TOUR_PLACE["The Venetian Macao"],
	8:WORLD_TOUR_PLACE["Casino de Monte Carlo"],
	9:WORLD_TOUR_PLACE["Twin Lions Casino"],
	10:WORLD_TOUR_PLACE["Casino Sochi"],
	11:WORLD_TOUR_PLACE["Crown Melbourne"],
	12:WORLD_TOUR_PLACE["Christchurch Casino"],
	13:WORLD_TOUR_PLACE["Trilenium Casino"],
	14:WORLD_TOUR_PLACE["Casino de Montréal"],
};
/*
export enum WORLD_TOUR_PLACE {
	Helsinki = 'Helsinki',
	Istanbul = 'Istanbul',
	KapDerGutenHoffnung = 'Kap der guten Hoffnung',
	KnuddelsHauptquartierKarlsruhe = 'Knuddels-Hauptquartier, Karlsruhe',
	Mallorca = 'Mallorca',
	Madrid = 'Madrid',
	Moskau = 'Moskau',
	NewYorkCity = 'New York City',
	Stonehenge = 'Stonehenge',
	Tunis = 'Tunis',
	AmundsenScottSüdpolstation = 'Amundsen-Scott-Südpolstation',
	AyersRock = 'AyersRock',
	BaikalseeIrkutsk = 'Baikalsee, Irkutsk',
	Bermudadreieck = 'Bermudadreieck',
	Kourou = 'Kourou',
	BikiniAtollMarshallinseln = 'Bikini-Atoll, Marshallinseln',
	BollywoodMumbai = 'Bollywood, Mumbai',
	BuenosAires = 'Buenos Aires',
	Casablanca = 'Casablanca',
	Daloa = 'Daloa',
	DetroitMichigan = 'Detroit, Michigan',
	Eyjafjallajökull = 'Eyjafjallajökull',
	Feuerland = 'Feuerland',
	Gizeh = 'Gizeh',
	GrandCanyonArizona = 'Grand Canyon, Arizona',
	Hanoi = 'Hanoi',
	Jamaica = 'Jamaica',
	Java = 'Java',
	Kilimandscharo = 'Kilimandscharo',
	Kinshasa = 'Kinshasa',
	Klondike = 'Klondike',
	KrakatauSundaInseln = 'Krakatau, Sunda-Inseln',
	Lhasa = 'Lhasa',
	Madagaskar = 'Madagaskar',
	MemphisTennessee = 'Memphis, Tennessee',
	MexikoStadt = 'Mexiko-Stadt',
	MiamiFlorida = 'Miami, Florida',
	NazcaLinien = 'Nazca-Linien',
	NewOrleansLouisiana = 'New Orleans, Louisiana',
	Osterinsel = 'Osterinsel',
	PanamaKanal = 'Panama-Kanal',
	Peking = 'Peking',
	Qasigiannguit = 'Qasigiannguit',
	RioDeJaneiro = 'Rio de Janeiro',
	Sanaa = 'Sanaa',
	SantaCruz = 'Santa Cruz',
	SeattleWashington = 'Seattle, Washington',
	SiliconValleyCalifornien = 'Silicon Valley, Californien',
	Sydney = 'Sydney',
	ThePalmDubai = 'The Palm, Dubai',
	Timbuktu = 'Timbuktu',
	Tokio = 'Tokio',
	TopekaKansas = 'Topeka, Kansas',
	Transsilvanien = 'Transsilvanien',
	TunguskaSibirien = 'Tunguska, Sibirien',
	VictoriaFalls = 'Victoria Falls',
	WaikikiHawaii = 'Waikiki, Hawaii',
	Wellington = 'Wellington',
	WrackDerTitanic = 'Wrack der Titanic',
	Atlantis = 'Atlantis',
}
*/

export const enum SlotPhase {
	stopped,
	spinning,
	stopping,
	payout,
}

export const enum FreegamePhase {
	symbolDraw,
	spinning,
}

export const enum BonusGamePhase {
	waiting,
	flying,
	payout,
}

export const enum GameState {
	normal,
	freeGames,
	bonusGames,
}

// -----------------
// Server --> Client

export const C_SLOT_RESULT: string = 'slotResult';
export interface P_SlotResult extends MySlotResult {}

export const C_SLOT_STATE: string = 'slotState';
export interface P_SlotState {
	gameState: GameState;
	slotPhase: SlotPhase;
	result: P_SlotResult;
	freegameState: P_FreeGameState;
	bonusGameState: P_BonusGameState;
	mostRecentBuyIn: number;
	knuddelAccount: number;
	cumulatedPayout: number;
}

export const C_FREE_GAME_STATE: string = 'slotFreeGameState';
export interface P_FreeGameState {
	phase: FreegamePhase;
	remainingFreeGames: number;
	freeGameSymbol: SlotSymbol;
	isDoublePayout: boolean;
}

export const C_BONUS_GAME_STATE: string = 'slotBonusGameState';
export interface P_BonusGameState {
	phase: BonusGamePhase;
	playerPositions: [];
	ownPosition: WORLD_TOUR_PLACE;
	amountOfKeys: number;
	remainingFlights: number;
	amountOfFragments: number;
	mostRecentChest: ChestType;
}

export const C_FLY_TO_DESTINATION: string = 'flyToDestination';
export interface P_FlyToDestination {
	destination: WORLD_TOUR_PLACE;
}

export const C_FREEGAME_SYMBOL_DRAW_RESULT: string = 'freegameSymbolDrawResult';
export interface P_FreegameSymbolDrawResult {
	drawnSymbol: SlotSymbol;
	otherSymbol1: SlotSymbol;
	otherSymbol2: SlotSymbol;
}

export const C_WELTREISE_UPDATE: string = 'weltreiseUpdate';
export interface P_WeltreiseUpdate {
	entries: [];
}

// ------------------
// Client --> Server

export const C_SLOT_UI_IS_READY: string = 'slotUiReady';
export interface P_SlotUIReady {
	// client tells server, that ui-creation is finished
	// server now can update diagram to current state (minimize problems with latency)
}

export const C_SPIN_BUTTON_CLICK: string = 'spinButtonClick';
export interface P_SpinButtonClick {
	amount: number;
}

export const C_DRAW_FREEGAME_SYMBOL: string = 'drawFreeGameSymbol';
export interface P_DrawFreegameSymbol {
	position: number;
}

export const C_SWITCH_TO_PAYOUT: string = 'switchToPayout';
export interface P_SlotStopped {}

export const C_SWITCH_TO_STOPPED: string = 'switchToStopped';
export interface P_SlotStopped {}


// -------------------------------------

export function numberFormatWithDecimal(numberRaw: any, decimals: number): string {
	if (numberRaw < 0) {
		const other = Math.floor(-numberRaw*Math.pow(10, decimals))/ Math.pow(10, decimals);
		if (other == 0) {
			return '0';
		} else {
			return '-' + numberFormatPos(other, decimals);
		}
	} else {
		return numberFormatPos(numberRaw, decimals);
	}
}

function numberFormatPos(numberRaw: any, decimals: number): string {
	let number: string = '' + Math.floor(numberRaw*Math.pow(10, decimals))/Math.pow(10, decimals);
	let minLength: number = 3;
	let array :string[] = number.split('.');
	if(array.length > 1) {
		minLength += array[1].length+1;
	}
	number = number.replace('.', ',');
	if (number.length > minLength) {
		var mod = number.length % minLength;
		var output = mod > 0 ? number.substring(0, mod) : '';
		for (let i = 0; i < Math.floor(number.length / minLength); i++) {
			if (mod == 0 && i == 0) output += number.substring(mod + minLength * i, mod + minLength * i + minLength);
			else output += '.' + number.substring(mod + minLength * i, mod + minLength * i + minLength);
		}
		return output;
	} else {
		return number;
	}
}
