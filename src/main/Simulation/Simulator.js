"use strict";
exports.__esModule = true;
var MySlotMachine_1 = require("../Slot/MySlotMachine");
var Configurations_1 = require("../Slot/Configurations");
var protocol_1 = require("../protocol");
var MySlotPictureInterpreter_1 = require("../Slot/MySlotPictureInterpreter");
var MySlotReel_1 = require("../Slot/MySlotReel");
var MySymbolPool_1 = require("../Slot/MySymbolPool");
var BonusGameManager_1 = require("../BonusGame/BonusGameManager");
var BonusGameMaps_1 = require("../BonusGame/BonusGameMaps");
var Simulator = /** @class */ (function () {
    function Simulator(account, buyIn, amountOfSpins, onlyFreegames, onlyFreegamesAmount, onlyNormalGames, disableBonusGames) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p;
        this.account = account;
        this.buyIn = buyIn;
        this.amountOfSpins = amountOfSpins;
        this.onlyFreegames = onlyFreegames;
        this.onlyFreegamesAmount = onlyFreegamesAmount;
        this.onlyNormalGames = onlyNormalGames;
        this.disableBonusGames = disableBonusGames;
        this.timesEnteredBonusGames = 0;
        this.timesEnteredFreeGames = 0;
        this.knuddelWonInFreeGamesWithoutFreegameSymbol = 0;
        this.knuddelWonInNormalGames = 0;
        this.spinsDone = 0;
        this.normalSpins = 0;
        this.freeGameSpins = 0;
        this.bonusGameSpins = 0;
        this.fragmentsFound = 0;
        this.extraKnuddelByDoublePayoutNormal = 0;
        this.extraKnuddelByDoublePayoutFreegame = 0;
        this.threeScatter = 0;
        this.fourScatter = 0;
        this.fiveScatter = 0;
        this.freegamesWonInNormalGame = 0;
        this.freegamesWonInFreeGame = 0;
        this.freegamesWonInBonusGame = 0;
        this.freegamesPerformance = (_a = {},
            _a[0] = 0,
            _a[10] = 0,
            _a[20] = 0,
            _a[30] = 0,
            _a[40] = 0,
            _a[50] = 0,
            _a[100] = 0,
            _a[150] = 0,
            _a[200] = 0,
            _a[500] = 0,
            _a[501] = 0,
            _a);
        this.appearanceFreegameSymbols = (_b = {},
            _b[protocol_1.MySlotSymbol.K1] = 0,
            _b[protocol_1.MySlotSymbol.K2] = 0,
            _b[protocol_1.MySlotSymbol.K3] = 0,
            _b[protocol_1.MySlotSymbol.K4] = 0,
            _b[protocol_1.MySlotSymbol.Ace] = 0,
            _b[protocol_1.MySlotSymbol.King] = 0,
            _b[protocol_1.MySlotSymbol.Queen] = 0,
            _b[protocol_1.MySlotSymbol.Jack] = 0,
            _b[protocol_1.MySlotSymbol.Ten] = 0,
            _b[protocol_1.MySlotSymbol.Scatter] = 0,
            _b[protocol_1.MySlotSymbol.Weltreise] = 0,
            _b[protocol_1.MySlotSymbol.Wild] = 0,
            _b);
        this.amountOfKnuddelWonInFreegamesPerSymbol = (_c = {},
            _c[protocol_1.MySlotSymbol.K1] = 0,
            _c[protocol_1.MySlotSymbol.K2] = 0,
            _c[protocol_1.MySlotSymbol.K3] = 0,
            _c[protocol_1.MySlotSymbol.K4] = 0,
            _c[protocol_1.MySlotSymbol.Ace] = 0,
            _c[protocol_1.MySlotSymbol.King] = 0,
            _c[protocol_1.MySlotSymbol.Queen] = 0,
            _c[protocol_1.MySlotSymbol.Jack] = 0,
            _c[protocol_1.MySlotSymbol.Ten] = 0,
            _c[protocol_1.MySlotSymbol.Scatter] = 0,
            _c[protocol_1.MySlotSymbol.Weltreise] = 0,
            _c[protocol_1.MySlotSymbol.Wild] = 0,
            _c);
        this.appearanceWinWith2KindOfSymbol = (_d = {},
            _d[protocol_1.MySlotSymbol.K1] = 0,
            _d[protocol_1.MySlotSymbol.K2] = 0,
            _d[protocol_1.MySlotSymbol.K3] = 0,
            _d[protocol_1.MySlotSymbol.K4] = 0,
            _d[protocol_1.MySlotSymbol.Ace] = 0,
            _d[protocol_1.MySlotSymbol.King] = 0,
            _d[protocol_1.MySlotSymbol.Queen] = 0,
            _d[protocol_1.MySlotSymbol.Jack] = 0,
            _d[protocol_1.MySlotSymbol.Ten] = 0,
            _d[protocol_1.MySlotSymbol.Scatter] = 0,
            _d[protocol_1.MySlotSymbol.Weltreise] = 0,
            _d[protocol_1.MySlotSymbol.Wild] = 0,
            _d);
        this.appearanceWinWith3KindOfSymbol = (_e = {},
            _e[protocol_1.MySlotSymbol.K1] = 0,
            _e[protocol_1.MySlotSymbol.K2] = 0,
            _e[protocol_1.MySlotSymbol.K3] = 0,
            _e[protocol_1.MySlotSymbol.K4] = 0,
            _e[protocol_1.MySlotSymbol.Ace] = 0,
            _e[protocol_1.MySlotSymbol.King] = 0,
            _e[protocol_1.MySlotSymbol.Queen] = 0,
            _e[protocol_1.MySlotSymbol.Jack] = 0,
            _e[protocol_1.MySlotSymbol.Ten] = 0,
            _e[protocol_1.MySlotSymbol.Scatter] = 0,
            _e[protocol_1.MySlotSymbol.Weltreise] = 0,
            _e[protocol_1.MySlotSymbol.Wild] = 0,
            _e);
        this.appearanceWinWith4KindOfSymbol = (_f = {},
            _f[protocol_1.MySlotSymbol.K1] = 0,
            _f[protocol_1.MySlotSymbol.K2] = 0,
            _f[protocol_1.MySlotSymbol.K3] = 0,
            _f[protocol_1.MySlotSymbol.K4] = 0,
            _f[protocol_1.MySlotSymbol.Ace] = 0,
            _f[protocol_1.MySlotSymbol.King] = 0,
            _f[protocol_1.MySlotSymbol.Queen] = 0,
            _f[protocol_1.MySlotSymbol.Jack] = 0,
            _f[protocol_1.MySlotSymbol.Ten] = 0,
            _f[protocol_1.MySlotSymbol.Scatter] = 0,
            _f[protocol_1.MySlotSymbol.Weltreise] = 0,
            _f[protocol_1.MySlotSymbol.Wild] = 0,
            _f);
        this.appearanceWinWith5KindOfSymbol = (_g = {},
            _g[protocol_1.MySlotSymbol.K1] = 0,
            _g[protocol_1.MySlotSymbol.K2] = 0,
            _g[protocol_1.MySlotSymbol.K3] = 0,
            _g[protocol_1.MySlotSymbol.K4] = 0,
            _g[protocol_1.MySlotSymbol.Ace] = 0,
            _g[protocol_1.MySlotSymbol.King] = 0,
            _g[protocol_1.MySlotSymbol.Queen] = 0,
            _g[protocol_1.MySlotSymbol.Jack] = 0,
            _g[protocol_1.MySlotSymbol.Ten] = 0,
            _g[protocol_1.MySlotSymbol.Scatter] = 0,
            _g[protocol_1.MySlotSymbol.Weltreise] = 0,
            _g[protocol_1.MySlotSymbol.Wild] = 0,
            _g);
        this.appearanceWinWith2KindOfSymbolInFreegames = (_h = {},
            _h[protocol_1.MySlotSymbol.K1] = 0,
            _h[protocol_1.MySlotSymbol.K2] = 0,
            _h[protocol_1.MySlotSymbol.K3] = 0,
            _h[protocol_1.MySlotSymbol.K4] = 0,
            _h[protocol_1.MySlotSymbol.Ace] = 0,
            _h[protocol_1.MySlotSymbol.King] = 0,
            _h[protocol_1.MySlotSymbol.Queen] = 0,
            _h[protocol_1.MySlotSymbol.Jack] = 0,
            _h[protocol_1.MySlotSymbol.Ten] = 0,
            _h[protocol_1.MySlotSymbol.Scatter] = 0,
            _h[protocol_1.MySlotSymbol.Weltreise] = 0,
            _h[protocol_1.MySlotSymbol.Wild] = 0,
            _h);
        this.appearanceWinWith3KindOfSymbolInFreegames = (_j = {},
            _j[protocol_1.MySlotSymbol.K1] = 0,
            _j[protocol_1.MySlotSymbol.K2] = 0,
            _j[protocol_1.MySlotSymbol.K3] = 0,
            _j[protocol_1.MySlotSymbol.K4] = 0,
            _j[protocol_1.MySlotSymbol.Ace] = 0,
            _j[protocol_1.MySlotSymbol.King] = 0,
            _j[protocol_1.MySlotSymbol.Queen] = 0,
            _j[protocol_1.MySlotSymbol.Jack] = 0,
            _j[protocol_1.MySlotSymbol.Ten] = 0,
            _j[protocol_1.MySlotSymbol.Scatter] = 0,
            _j[protocol_1.MySlotSymbol.Weltreise] = 0,
            _j[protocol_1.MySlotSymbol.Wild] = 0,
            _j);
        this.appearanceWinWith4KindOfSymbolInFreegames = (_k = {},
            _k[protocol_1.MySlotSymbol.K1] = 0,
            _k[protocol_1.MySlotSymbol.K2] = 0,
            _k[protocol_1.MySlotSymbol.K3] = 0,
            _k[protocol_1.MySlotSymbol.K4] = 0,
            _k[protocol_1.MySlotSymbol.Ace] = 0,
            _k[protocol_1.MySlotSymbol.King] = 0,
            _k[protocol_1.MySlotSymbol.Queen] = 0,
            _k[protocol_1.MySlotSymbol.Jack] = 0,
            _k[protocol_1.MySlotSymbol.Ten] = 0,
            _k[protocol_1.MySlotSymbol.Scatter] = 0,
            _k[protocol_1.MySlotSymbol.Weltreise] = 0,
            _k[protocol_1.MySlotSymbol.Wild] = 0,
            _k);
        this.appearanceWinWith5KindOfSymbolInFreegames = (_l = {},
            _l[protocol_1.MySlotSymbol.K1] = 0,
            _l[protocol_1.MySlotSymbol.K2] = 0,
            _l[protocol_1.MySlotSymbol.K3] = 0,
            _l[protocol_1.MySlotSymbol.K4] = 0,
            _l[protocol_1.MySlotSymbol.Ace] = 0,
            _l[protocol_1.MySlotSymbol.King] = 0,
            _l[protocol_1.MySlotSymbol.Queen] = 0,
            _l[protocol_1.MySlotSymbol.Jack] = 0,
            _l[protocol_1.MySlotSymbol.Ten] = 0,
            _l[protocol_1.MySlotSymbol.Scatter] = 0,
            _l[protocol_1.MySlotSymbol.Weltreise] = 0,
            _l[protocol_1.MySlotSymbol.Wild] = 0,
            _l);
        this.appearanceOfChestTypes = (_m = {},
            _m[protocol_1.ChestType.Normal] = 0,
            _m[protocol_1.ChestType.Golden] = 0,
            _m[protocol_1.ChestType.Giant] = 0,
            _m);
        this.knuddelWonByChestType = (_o = {},
            _o[protocol_1.ChestType.Normal] = 0,
            _o[protocol_1.ChestType.Golden] = 0,
            _o[protocol_1.ChestType.Giant] = 0,
            _o);
        this.freegamesWonByChestType = (_p = {},
            _p[protocol_1.ChestType.Normal] = 0,
            _p[protocol_1.ChestType.Golden] = 0,
            _p[protocol_1.ChestType.Giant] = 0,
            _p);
        // ------------------------------------------------------------------------------------------------
        this.bonusGameManager = new BonusGameManager_1.BonusGameManager();
        this.stop = false;
        this.doublePayoutSpins = 0;
        this.cummulatedFreegamePayout = 0;
        this.normalSpinsWithoutPayout = 0;
        this.slotMachine = new MySlotMachine_1.MySlotMachine(this.createStartConfiguration(), Configurations_1.FreegameSymbolProbability, protocol_1.SLOT_SYMBOL_MAP);
        this.startAmount = account;
        this.myInterpreter = this.initializeInterpreter();
        this.initializeState(this.slotMachine.getCurrentPicture());
    }
    Simulator.prototype.simulateSpin = function () {
        while (!this.stop && this.spinsDone < this.amountOfSpins) {
            if (this.spinsDone % 10000 == 0) {
                console.log(this.spinsDone);
            }
            this.spinsDone++;
            this.handleSpinButtonClick(this.buyIn);
        }
        this.printResult();
    };
    Simulator.prototype.createStartConfiguration = function () {
        return [
            new MySlotReel_1.MySlotReel(Configurations_1.START_CONFIGURATION, new MySymbolPool_1.MySymbolPool(Configurations_1.FIRST_REEL_SYMBOL_POOL)),
            new MySlotReel_1.MySlotReel(Configurations_1.START_CONFIGURATION, new MySymbolPool_1.MySymbolPool(Configurations_1.SECOND_REEL_SYMBOL_POOL)),
            new MySlotReel_1.MySlotReel(Configurations_1.START_CONFIGURATION, new MySymbolPool_1.MySymbolPool(Configurations_1.THIRD_REEL_SYMBOL_POOL)),
            new MySlotReel_1.MySlotReel(Configurations_1.START_CONFIGURATION, new MySymbolPool_1.MySymbolPool(Configurations_1.FOURTH_REEL_SYMBOL_POOL)),
            new MySlotReel_1.MySlotReel(Configurations_1.START_CONFIGURATION, new MySymbolPool_1.MySymbolPool(Configurations_1.FIFTH_REEL_SYMBOL_POOL)),
        ];
    };
    Simulator.prototype.initializeInterpreter = function () {
        return new MySlotPictureInterpreter_1.MySlotPictureInterpreter(protocol_1.MY_PAY_LINES, protocol_1.MULTIPLIER_MAP, protocol_1.SLOT_SYMBOL_MAP[protocol_1.MySlotSymbol.Scatter], protocol_1.FREEGAMES_MAP, protocol_1.SLOT_SYMBOL_MAP[protocol_1.MySlotSymbol.Wild], protocol_1.SLOT_SYMBOL_MAP[protocol_1.MySlotSymbol.Weltreise], protocol_1.BONUS_GAME_MAP, Configurations_1.KEY_FRAGMENT_PROBABILITY);
    };
    Simulator.prototype.initializeState = function (reels) {
        this.state = {
            gameState: 0 /* normal */,
            slotPhase: 0 /* stopped */,
            result: {
                fragmentMatrix: null,
                amountOfFragments: 0,
                payout: 0,
                wins: [],
                freeGamePayout: 0,
                bonusGamesWon: 0,
                slotPicture: reels.map(function (reel) { return reel.getSymbols(); }),
                freeGamesWon: 0
            },
            mostRecentBuyIn: 0,
            knuddelAccount: 0,
            cumulatedPayout: 0,
            freegameState: {
                phase: 0 /* symbolDraw */,
                remainingFreeGames: 0,
                freeGameSymbol: null,
                isDoublePayout: false
            },
            bonusGameState: {
                phase: 1 /* flying */,
                ownPosition: null,
                playerPositions: [],
                remainingFlights: 0,
                amountOfFragments: 0,
                amountOfKeys: 0,
                mostRecentChest: null
            }
        };
    };
    Simulator.prototype.startNormalGame = function (buyInAmount) {
        var spinResult = this.myInterpreter.interpretResult(this.slotMachine.spin(), buyInAmount);
        if (spinResult.payout == 0) {
            this.normalSpinsWithoutPayout++;
        }
        this.trackNormalGameResult2(spinResult);
        this.updateStateAfterSpin(spinResult);
    };
    Simulator.prototype.startBonusGame = function (buyInAmount) {
        var flightResult = this.bonusGameManager.startRound(this.state.bonusGameState);
        this.trackBonusGameResult(flightResult);
        this.updateStateAfterBonusGame(flightResult, buyInAmount);
    };
    Simulator.prototype.startFeeGame = function (buyInAmount) {
        this.slotMachine.addSymbol(this.state.freegameState.freeGameSymbol);
        var spinResult = this.myInterpreter.interpretResultAsFreeGameSpin(this.slotMachine.spin(), buyInAmount, this.state.freegameState.freeGameSymbol);
        this.cummulatedFreegamePayout += spinResult.freeGamePayout;
        this.trackFreegameResult2(spinResult);
        this.updateStateAfterFreegame(spinResult);
    };
    Simulator.prototype.updateStateAfterBonusGame = function (flightResult, buyInAmount) {
        this.updateResult({
            freeGamesWon: flightResult.chestContent.freegames,
            bonusGamesWon: 0,
            wins: [],
            slotPicture: this.state.result.slotPicture,
            freeGamePayout: 0,
            fragmentMatrix: this.state.result.fragmentMatrix,
            payout: flightResult.chestContent.knuddel * buyInAmount,
            amountOfFragments: 0
        });
        this.updateBonusGameState(0, flightResult);
        this.updateFreeGameState(false, flightResult.chestContent.freegames);
    };
    Simulator.prototype.updateStateAfterFreegame = function (spinResult) {
        this.state.freegameState.remainingFreeGames--;
        this.updateStateAfterSpin(spinResult);
    };
    Simulator.prototype.updateStateAfterSpin = function (spinResult) {
        //let isDoublePayout: boolean = spinResult.freeGamesWon == FREEGAMES_MAP[5];
        var isDoublePayout = false;
        if (this.state.freegameState.isDoublePayout) {
            spinResult = this.doublePayout(spinResult);
        }
        this.updateFragments(spinResult);
        this.updateResult(spinResult);
        this.updateBonusGameState(spinResult.bonusGamesWon);
        this.updateFreeGameState(isDoublePayout, spinResult.freeGamesWon);
    };
    Simulator.prototype.updateFragments = function (spinResult) {
        var fragments = spinResult.amountOfFragments + this.state.bonusGameState.amountOfFragments;
        if (fragments >= protocol_1.FRAGMENTS_NEEDED_FOR_KEY) {
            this.state.bonusGameState.amountOfKeys++;
            fragments -= protocol_1.FRAGMENTS_NEEDED_FOR_KEY;
        }
        this.state.bonusGameState.amountOfFragments = fragments;
    };
    Simulator.prototype.updateResult = function (spinResult) {
        this.state.result = spinResult;
    };
    Simulator.prototype.updateFreeGameState = function (isDoublePayout, wonFreeGames) {
        switch (this.state.gameState) {
            case 1 /* freeGames */:
                this.freegamesWonInFreeGame += wonFreeGames;
                break;
            case 2 /* bonusGames */:
                this.freegamesWonInBonusGame += wonFreeGames;
                break;
            case 0 /* normal */:
                this.freegamesWonInNormalGame += wonFreeGames;
                break;
        }
        var isStillDoublePayout = isDoublePayout;
        var phase = this.state.freegameState.phase;
        if (this.state.freegameState.remainingFreeGames == 0 && wonFreeGames == 0) {
            isStillDoublePayout = false;
        }
        if (isDoublePayout) {
            this.state.freegameState.isDoublePayout = true;
        }
        if (this.onlyFreegames && this.state.freegameState.remainingFreeGames > 0) {
            wonFreeGames = 0;
        }
        this.state.freegameState = {
            remainingFreeGames: this.state.freegameState.remainingFreeGames + wonFreeGames,
            freeGameSymbol: this.state.freegameState.freeGameSymbol,
            isDoublePayout: isStillDoublePayout,
            phase: phase
        };
    };
    Simulator.prototype.updateBonusGameState = function (wonBonusGames, flightResult) {
        var amountOfKeys = this.state.bonusGameState.amountOfKeys;
        if (flightResult && this.state.bonusGameState.amountOfKeys > 0) {
            amountOfKeys--;
        }
        this.state.bonusGameState = {
            phase: 1 /* flying */,
            amountOfKeys: amountOfKeys,
            amountOfFragments: this.state.bonusGameState.amountOfFragments,
            ownPosition: null,
            playerPositions: [],
            remainingFlights: this.disableBonusGames ? 0 : (flightResult
                ? this.state.bonusGameState.remainingFlights - 1
                : this.state.bonusGameState.remainingFlights + wonBonusGames),
            mostRecentChest: flightResult ? flightResult.chestType : this.state.bonusGameState.mostRecentChest
        };
    };
    Simulator.prototype.updateGameState = function () {
        if (this.onlyFreegames && this.state.freegameState.remainingFreeGames == 0) {
            this.state.freegameState.remainingFreeGames = this.onlyFreegamesAmount;
        }
        if (this.state.bonusGameState.remainingFlights > 0 && !this.onlyNormalGames && !this.onlyFreegames) {
            this.state.gameState = 2 /* bonusGames */;
        }
        else if (this.state.freegameState.remainingFreeGames > 0 && !this.onlyNormalGames) {
            this.state.gameState = 1 /* freeGames */;
        }
        else {
            this.state.gameState = 0 /* normal */;
        }
    };
    Simulator.prototype.handleSpinButtonClick = function (buyIn) {
        switch (this.state.gameState) {
            case 0 /* normal */:
                this.handleButtonClickInNormalGame(buyIn);
                break;
            case 2 /* bonusGames */:
                this.handleButtonClickInBonusGame(buyIn);
                break;
            case 1 /* freeGames */:
                this.handleButtonClickInFreeGame(buyIn);
                break;
        }
        this.updateGameState();
    };
    Simulator.prototype.handleButtonClickInNormalGame = function (buyIn) {
        this.startNormalGame(buyIn);
    };
    Simulator.prototype.handleButtonClickInBonusGame = function (buyIn) {
        this.startBonusGame(buyIn);
    };
    Simulator.prototype.handleButtonClickInFreeGame = function (buyIn) {
        switch (this.state.freegameState.phase) {
            case 0 /* symbolDraw */:
                this.handleFreegameSymbolDraw();
                this.state.freegameState.phase = 1 /* spinning */;
                this.handleButtonClickInFreeGame(buyIn);
                break;
            case 1 /* spinning */:
                this.startFeeGame(buyIn);
                if (this.state.freegameState.remainingFreeGames == 0) {
                    this.trackFreegamePerformace(this.buyIn);
                    this.state.freegameState.phase = 0 /* symbolDraw */;
                }
        }
    };
    Simulator.prototype.handleFreegameSymbolDraw = function () {
        var freeGameSymbol = this.slotMachine.drawFreegameSymbol();
        this.state.freegameState.freeGameSymbol = freeGameSymbol;
        this.trackFreeGameSymbol(protocol_1.MySlotSymbol[freeGameSymbol.name]);
    };
    Simulator.prototype.doublePayout = function (spinResult) {
        return {
            amountOfFragments: spinResult.amountOfFragments,
            payout: spinResult.payout * 2,
            fragmentMatrix: spinResult.fragmentMatrix,
            freeGamePayout: spinResult.freeGamePayout * 2,
            slotPicture: spinResult.slotPicture,
            wins: spinResult.wins.map(function (w) {
                return {
                    amount: w.amount * 2,
                    symbol: w.symbol,
                    matches: w.matches,
                    winningLine: w.winningLine
                };
            }),
            bonusGamesWon: spinResult.bonusGamesWon,
            freeGamesWon: spinResult.freeGamesWon
        };
    };
    Simulator.prototype.printResult = function () {
        var knuddelWonInFreeGames = this.knuddelWonInFreeGamesWithoutFreegameSymbol;
        var freegamesSymbolKnuddelDistributionHeading = '\n';
        var freegamesSymbolKnuddelDistributionContent = '\n';
        var appearenceOfFreegameSymbol = '\n';
        var appearanceWinWith2KindOfSymbolString = '\n';
        var appearanceWinWith3KindOfSymbolString = '\n';
        var appearanceWinWith4KindOfSymbolString = '\n';
        var appearanceWinWith5KindOfSymbolString = '\n';
        var appearanceWinWith2KindOfSymbolInFreegamesString = '\n';
        var appearanceWinWith3KindOfSymbolInFreegamesString = '\n';
        var appearanceWinWith4KindOfSymbolInFreegamesString = '\n';
        var appearanceWinWith5KindOfSymbolInFreegamesString = '\n';
        var knuddelWonInBonusGames = this.knuddelWonByChestType[protocol_1.ChestType.Normal] +
            this.knuddelWonByChestType[protocol_1.ChestType.Golden] +
            this.knuddelWonByChestType[protocol_1.ChestType.Giant];
        for (var key in this.amountOfKnuddelWonInFreegamesPerSymbol) {
            knuddelWonInFreeGames += this.amountOfKnuddelWonInFreegamesPerSymbol[key];
            freegamesSymbolKnuddelDistributionHeading = freegamesSymbolKnuddelDistributionHeading + key + '","';
            appearenceOfFreegameSymbol = appearenceOfFreegameSymbol + this.appearanceFreegameSymbols[key] + '","';
            freegamesSymbolKnuddelDistributionContent =
                freegamesSymbolKnuddelDistributionContent + this.amountOfKnuddelWonInFreegamesPerSymbol[key] + '","';
            appearanceWinWith2KindOfSymbolString =
                appearanceWinWith2KindOfSymbolString + this.appearanceWinWith2KindOfSymbol[key] + '","';
            appearanceWinWith3KindOfSymbolString =
                appearanceWinWith3KindOfSymbolString + this.appearanceWinWith3KindOfSymbol[key] + '","';
            appearanceWinWith4KindOfSymbolString =
                appearanceWinWith4KindOfSymbolString + this.appearanceWinWith4KindOfSymbol[key] + '","';
            appearanceWinWith5KindOfSymbolString =
                appearanceWinWith5KindOfSymbolString + this.appearanceWinWith5KindOfSymbol[key] + '","';
            appearanceWinWith2KindOfSymbolInFreegamesString =
                appearanceWinWith2KindOfSymbolInFreegamesString +
                    this.appearanceWinWith2KindOfSymbolInFreegames[key] +
                    '","';
            appearanceWinWith3KindOfSymbolInFreegamesString =
                appearanceWinWith3KindOfSymbolInFreegamesString +
                    this.appearanceWinWith3KindOfSymbolInFreegames[key] +
                    '","';
            appearanceWinWith4KindOfSymbolInFreegamesString =
                appearanceWinWith4KindOfSymbolInFreegamesString +
                    this.appearanceWinWith4KindOfSymbolInFreegames[key] +
                    '","';
            appearanceWinWith5KindOfSymbolInFreegamesString =
                appearanceWinWith5KindOfSymbolInFreegamesString +
                    this.appearanceWinWith5KindOfSymbolInFreegames[key] +
                    '","';
        }
        var payIn = this.normalSpins * this.buyIn;
        var returnNormalGames = (this.knuddelWonInNormalGames / payIn) * 100;
        var returnFreeGames = (knuddelWonInFreeGames / payIn) * 100;
        var returnBonusGames = (knuddelWonInBonusGames / payIn) * 100;
        var returnTotal = returnNormalGames + returnBonusGames + returnFreeGames;
        var returnFreegamesNormalSpins = (this.knuddelWonInFreeGamesWithoutFreegameSymbol / payIn) * 100;
        var returnFreegamesFeatureSpins = returnFreeGames - returnFreegamesNormalSpins;
        var freegameSpinsFromBonusGame = this.freegamesWonByChestType[protocol_1.ChestType.Normal] +
            this.freegamesWonByChestType[protocol_1.ChestType.Giant] +
            this.freegamesWonByChestType[protocol_1.ChestType.Golden];
        var estimatedReturnByBonusGameFreegames = (freegameSpinsFromBonusGame / this.freeGameSpins) * returnFreeGames;
        var estimatedReturnByNormalFreegames = returnFreeGames - estimatedReturnByBonusGameFreegames;
        freegamesSymbolKnuddelDistributionHeading = freegamesSymbolKnuddelDistributionHeading + '';
        var golden = '';
        var normal = '';
        var giant = '';
        for (var i = 0; i <= 6; i++) {
            golden += 'level","' + i + '","content","' + JSON.stringify(BonusGameMaps_1.GOLDEN_CHEST_LEVEL_TO_CONTENT_MAP[i]) + '","Anzahl:","' + BonusGameManager_1.GOLDEN_CHEST_LEVEL_TO_FREQUENCY[i] + '\n';
            normal += 'level","' + i + '","content","' + JSON.stringify(BonusGameMaps_1.NORMAL_CHEST_LEVEL_TO_CONTENT_MAP[i]) + '","Anzahl:","' + BonusGameManager_1.NORMAL_CHEST_LEVEL_TO_FREQUENCY[i] + '\n';
            giant += 'level","' + i + '","content","' + JSON.stringify(BonusGameMaps_1.GIANT_CHEST_LEVEL_TO_CONTENT_MAP[i]) + '","Anzahl:","' + BonusGameManager_1.GIANT_CHEST_LEVEL_TO_FREQUENCY[i] + '\n';
        }
        //Freegamesperformance
        var freegamesPerformance = 'Freegamesperformance:\n' +
            'Auszahlung = 0","' + this.freegamesPerformance[0] / this.timesEnteredFreeGames * 100 + '%\n' +
            'Auszahlung <= 10","' + this.freegamesPerformance[10] / this.timesEnteredFreeGames * 100 + '%\n' +
            'Auszahlung <= 20","' + this.freegamesPerformance[20] / this.timesEnteredFreeGames * 100 + '%\n' +
            'Auszahlung <= 30","' + this.freegamesPerformance[30] / this.timesEnteredFreeGames * 100 + '%\n' +
            'Auszahlung <= 40","' + this.freegamesPerformance[40] / this.timesEnteredFreeGames * 100 + '%\n' +
            'Auszahlung <= 50","' + this.freegamesPerformance[50] / this.timesEnteredFreeGames * 100 + '%\n' +
            'Auszahlung <= 100","' + this.freegamesPerformance[100] / this.timesEnteredFreeGames * 100 + '%\n' +
            'Auszahlung <= 150","' + this.freegamesPerformance[150] / this.timesEnteredFreeGames * 100 + '%\n' +
            'Auszahlung <= 200","' + this.freegamesPerformance[200] / this.timesEnteredFreeGames * 100 + '%\n' +
            'Auszahlung <= 500","' + this.freegamesPerformance[500] / this.timesEnteredFreeGames * 100 + '%\n' +
            'Auszahlung > 500","' + this.freegamesPerformance[501] / this.timesEnteredFreeGames * 100 + '%\n';
        var output = '\n';
        output =
            output +
                'Anzahl der Spins:",' +
                this.spinsDone +
                ',"Normal:",' +
                this.normalSpins +
                ',"Freispiel:",' +
                this.freeGameSpins +
                ',"Bonus","' +
                this.bonusGameSpins +
                '\n' +
                'Spins mit double Payout","' +
                this.doublePayoutSpins +
                '\n' +
                'Normale Spins ohne Payout","' +
                (this.normalSpinsWithoutPayout / this.normalSpins) * 100 + '%' +
                '\n' +
                'Guthaben:,' +
                this.account +
                '\n' +
                'Gesammelte Schlüsselfragments:,' +
                this.fragmentsFound +
                '\n' +
                'Anzahl der gestarteten Bonusspiele:,' +
                this.timesEnteredBonusGames +
                '\n' +
                'Anzahl der gestarteten Freipiele:,' +
                this.timesEnteredFreeGames +
                '\n' +
                'Gewonne Knuddel in normalen Spins:,' +
                this.knuddelWonInNormalGames +
                '\n' +
                'In Freispielen gewonnene Knuddel:,' +
                knuddelWonInFreeGames +
                '","Davon mit normalen Spins","' +
                this.knuddelWonInFreeGamesWithoutFreegameSymbol +
                '\n' +
                '"In Bonusspielen gewonnene Knuddel","' +
                knuddelWonInBonusGames +
                '\n' +
                '\n' +
                '\n' +
                'Auszahlung von Knuddel nach Freispielsymbolen","' +
                freegamesSymbolKnuddelDistributionHeading +
                freegamesSymbolKnuddelDistributionContent +
                '\n' +
                '\n' +
                'Häufigkeit von Freispielsymbolen' +
                freegamesSymbolKnuddelDistributionHeading +
                appearenceOfFreegameSymbol +
                '\n' +
                '\n' +
                '\n' +
                'Häufigkeit von Gewinnkombinationen (2er,3er,4er,5er)' +
                freegamesSymbolKnuddelDistributionHeading +
                appearanceWinWith2KindOfSymbolString +
                appearanceWinWith3KindOfSymbolString +
                appearanceWinWith4KindOfSymbolString +
                appearanceWinWith5KindOfSymbolString +
                '\n' +
                '\n' +
                '\n' +
                'Häufigkeit von Scattern\n' +
                '3","4","5' +
                '\n' +
                this.threeScatter +
                '","' +
                this.fourScatter +
                '","' +
                this.fiveScatter +
                '\n' +
                '\n' +
                '\n' +
                'Häufigkeit von Gewinnkombinationen in Freispielen (2er,3er,4er,5er)' +
                freegamesSymbolKnuddelDistributionHeading +
                appearanceWinWith2KindOfSymbolInFreegamesString +
                appearanceWinWith3KindOfSymbolInFreegamesString +
                appearanceWinWith4KindOfSymbolInFreegamesString +
                appearanceWinWith5KindOfSymbolInFreegamesString +
                '\n' +
                '\n' +
                '\n' +
                '\n' +
                'Truhentypen' +
                '","' +
                'Normal' +
                '","' +
                'Golden' +
                '","' +
                'Riesen' +
                '\n' +
                'Häufigkeit' +
                '","' +
                this.appearanceOfChestTypes[protocol_1.ChestType.Normal] +
                '","' +
                this.appearanceOfChestTypes[protocol_1.ChestType.Golden] +
                '","' +
                this.appearanceOfChestTypes[protocol_1.ChestType.Giant] +
                '\n' +
                '\n' +
                normal +
                '\n' +
                golden +
                '\n' +
                giant +
                '\n' +
                'Gewonnene Knuddel' +
                '","' +
                this.knuddelWonByChestType[protocol_1.ChestType.Normal] +
                '","' +
                this.knuddelWonByChestType[protocol_1.ChestType.Golden] +
                '","' +
                this.knuddelWonByChestType[protocol_1.ChestType.Giant] +
                '\n' +
                'Gewonnene Freispiele' +
                '","' +
                this.freegamesWonByChestType[protocol_1.ChestType.Normal] +
                '","' +
                this.freegamesWonByChestType[protocol_1.ChestType.Golden] +
                '","' +
                this.freegamesWonByChestType[protocol_1.ChestType.Giant] +
                '\n' +
                '\n' +
                'Return Total","' +
                returnTotal +
                '%' +
                '\n' +
                'Normalspiel","' +
                returnNormalGames +
                '%' +
                '\n' +
                'Freispiele","' +
                returnFreeGames +
                '%' +
                '\n' +
                'Bonusspiele(nur Knuddel)","' +
                returnBonusGames +
                '%' +
                '\n' +
                'Freispiele normale","' +
                returnFreegamesNormalSpins +
                '%' +
                '\n' +
                'Freispiele Sonderdrehs","' +
                returnFreegamesFeatureSpins +
                '%' +
                '\n' +
                'Freispiele durch normale Drehs","' +
                estimatedReturnByNormalFreegames +
                '%' +
                '\n' +
                'Freispiele durch Bonusspiele","' +
                estimatedReturnByBonusGameFreegames +
                '%' +
                '\n' +
                'Zusätzlich durch doppelte Auszahlung erhaltene Knuddel","' +
                this.extraKnuddelByDoublePayoutNormal +
                '","normal","' +
                this.extraKnuddelByDoublePayoutFreegame +
                '","freegame' +
                '\n' + freegamesPerformance +
                '\n' +
                'freespins won in normalgame","' +
                this.freegamesWonInNormalGame +
                '\n' +
                'freespins won in freegame","' +
                this.freegamesWonInFreeGame +
                '\n' +
                'freespins won in bonusgame","' +
                this.freegamesWonInBonusGame;
        output = this.convertToCSV(output);
        console.log('Ergebniss nach ' + this.spinsDone + ' Spins', output);
    };
    // ------- TRACKING --------
    Simulator.prototype.trackWinCombination = function (amount, symbol) {
        if (amount == 2) {
            this.appearanceWinWith2KindOfSymbol[symbol]++;
        }
        else if (amount == 3) {
            this.appearanceWinWith3KindOfSymbol[symbol]++;
        }
        else if (amount == 4) {
            this.appearanceWinWith4KindOfSymbol[symbol]++;
        }
        else if (amount == 5) {
            this.appearanceWinWith5KindOfSymbol[symbol]++;
        }
    };
    Simulator.prototype.trackWinCombinationInFreegames = function (amount, symbol) {
        if (amount == 2) {
            this.appearanceWinWith2KindOfSymbolInFreegames[symbol]++;
        }
        else if (amount == 3) {
            this.appearanceWinWith3KindOfSymbolInFreegames[symbol]++;
        }
        else if (amount == 4) {
            this.appearanceWinWith4KindOfSymbolInFreegames[symbol]++;
        }
        else if (amount == 5) {
            this.appearanceWinWith5KindOfSymbolInFreegames[symbol]++;
        }
    };
    Simulator.prototype.trackFreeGameSymbol = function (symbol) {
        this.appearanceFreegameSymbols[symbol]++;
        this.increaseFreegamesEntered();
    };
    Simulator.prototype.trackBonusGame = function (chestType, knuddelWon, amountOfFreegamesWon) {
        this.appearanceOfChestTypes[chestType]++;
        this.knuddelWonByChestType[chestType] += knuddelWon;
        this.freegamesWonByChestType[chestType] += amountOfFreegamesWon;
        this.account += knuddelWon;
        this.bonusGameSpins++;
    };
    Simulator.prototype.trackFreegameResult = function (freegameSymbol, spinResult, isDoublePayout) {
        if (isDoublePayout) {
            this.extraKnuddelByDoublePayoutNormal += spinResult.payout;
            this.extraKnuddelByDoublePayoutFreegame += spinResult.freeGamePayout;
            this.doublePayoutSpins++;
        }
        var mulitplier = isDoublePayout ? 2 : 1;
        this.amountOfKnuddelWonInFreegamesPerSymbol[freegameSymbol] += spinResult.freeGamePayout * mulitplier;
        this.knuddelWonInFreeGamesWithoutFreegameSymbol += spinResult.payout * mulitplier;
        this.account += spinResult.freeGamePayout + spinResult.payout;
        this.freeGameSpins++;
    };
    Simulator.prototype.trackNormalGameResult = function (knuddelWon) {
        this.knuddelWonInNormalGames += knuddelWon;
        this.account += knuddelWon - this.buyIn;
        this.normalSpins++;
    };
    Simulator.prototype.increaseFreegamesEntered = function () {
        this.timesEnteredFreeGames++;
    };
    Simulator.prototype.increaseCountOfBonusGameEntrances = function () {
        this.timesEnteredBonusGames++;
    };
    Simulator.prototype.stopSimulation = function () {
        this.stop = true;
    };
    Simulator.prototype.trackFragments = function (keyFragmentsFound) {
        this.fragmentsFound += keyFragmentsFound;
    };
    Simulator.prototype.trackNormalGameResult2 = function (spinResult) {
        var _this = this;
        this.trackNormalGameResult(spinResult.payout);
        spinResult.wins.forEach(function (w) { return _this.trackWinCombination(w.matches, protocol_1.MySlotSymbol[w.symbol.name]); });
        this.trackFragments(spinResult.amountOfFragments);
        if (spinResult.freeGamesWon == protocol_1.FREEGAMES_MAP[3]) {
            this.threeScatter++;
        }
        if (spinResult.freeGamesWon == protocol_1.FREEGAMES_MAP[4]) {
            this.fourScatter++;
        }
        if (spinResult.freeGamesWon == protocol_1.FREEGAMES_MAP[5]) {
            this.fiveScatter++;
        }
    };
    Simulator.prototype.trackBonusGameResult = function (flightResult) {
        if (this.state.bonusGameState.remainingFlights == 1) {
            this.increaseCountOfBonusGameEntrances();
        }
        this.trackBonusGame(flightResult.chestType, flightResult.chestContent.knuddel, flightResult.chestContent.freegames);
    };
    Simulator.prototype.trackFreegameResult2 = function (spinResult) {
        var _this = this;
        this.trackFreegameResult(protocol_1.MySlotSymbol[this.state.freegameState.freeGameSymbol.name], spinResult, this.state.freegameState.isDoublePayout);
        spinResult.wins.forEach(function (w) { return _this.trackWinCombinationInFreegames(w.matches, protocol_1.MySlotSymbol[w.symbol.name]); });
    };
    Simulator.prototype.convertToCSV = function (output) {
        var returnValue = output.split('\n').join('"\n"');
        returnValue = returnValue.split(':,').join(':","');
        returnValue = returnValue.split('.').join(',');
        return returnValue;
    };
    Simulator.prototype.trackFreegamePerformace = function (buyIn) {
        var factor = this.cummulatedFreegamePayout;
        if (factor == 0) {
            this.freegamesPerformance[0] += 1;
        }
        else if (factor <= 10) {
            this.freegamesPerformance[10] += 1;
        }
        else if (factor <= 20) {
            this.freegamesPerformance[20] += 1;
        }
        else if (factor <= 30) {
            this.freegamesPerformance[30] += 1;
        }
        else if (factor <= 40) {
            this.freegamesPerformance[40] += 1;
        }
        else if (factor <= 50) {
            this.freegamesPerformance[50] += 1;
        }
        else if (factor <= 100) {
            this.freegamesPerformance[100] += 1;
        }
        else if (factor <= 150) {
            this.freegamesPerformance[150] += 1;
        }
        else if (factor <= 200) {
            this.freegamesPerformance[200] += 1;
        }
        else if (factor <= 500) {
            this.freegamesPerformance[500] += 1;
        }
        else {
            this.freegamesPerformance[501] += 1;
        }
        this.cummulatedFreegamePayout = 0;
    };
    return Simulator;
}());
exports.Simulator = Simulator;
