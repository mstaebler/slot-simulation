import {MySlotMachine} from '../Slot/MySlotMachine';
import {
	FIFTH_REEL_SYMBOL_POOL,
	FIRST_REEL_SYMBOL_POOL,
	FOURTH_REEL_SYMBOL_POOL,
	FreegameSymbolProbability,
	KEY_FRAGMENT_PROBABILITY,
	SECOND_REEL_SYMBOL_POOL,
	START_CONFIGURATION,
	THIRD_REEL_SYMBOL_POOL,
} from '../Slot/Configurations';
import {
	BONUS_GAME_MAP,
	BonusGamePhase,
	ChestType, FRAGMENTS_NEEDED_FOR_KEY,
	FreegamePhase,
	FREEGAMES_MAP,
	GameState,
	MULTIPLIER_MAP,
	MY_PAY_LINES,
	MySlotSymbol,
	P_SlotResult,
	P_SlotState,
	SLOT_SYMBOL_MAP,
	SlotPhase,
} from '../protocol';
import {MySlotPictureInterpreter} from '../Slot/MySlotPictureInterpreter';
import {MySlotReel} from '../Slot/MySlotReel';
import {MySymbolPool} from '../Slot/MySymbolPool';
import {SlotReel} from '../../SlotAPI/SlotMachine/SlotReel';
import {MySlotResult} from '../Slot/MySlotResult';
import {
	BonusGameManager,
	BonusRoundResult, GIANT_CHEST_LEVEL_TO_FREQUENCY,
	GOLDEN_CHEST_LEVEL_TO_FREQUENCY,
	NORMAL_CHEST_LEVEL_TO_FREQUENCY
} from '../BonusGame/BonusGameManager';
import {SlotSymbol} from '../../SlotAPI/SlotConfigurators/SlotSymbol';
import {
	GIANT_CHEST_LEVEL_TO_CONTENT_MAP,
	GOLDEN_CHEST_LEVEL_TO_CONTENT_MAP,
	NORMAL_CHEST_LEVEL_TO_CONTENT_MAP
} from "../BonusGame/BonusGameMaps";


interface trackMyFreegames {
	didPayout: number;
	spins: number;
}

export class Simulator {
	private timesEnteredBonusGames: number = 0;
	private timesEnteredFreeGames: number = 0;
	private knuddelWonInFreeGamesWithoutFreegameSymbol: number = 0;
	private knuddelWonInNormalGames: number = 0;
	private spinsDone: number = 0;
	private startAmount: number;
	private normalSpins: number = 0;
	private freeGameSpins: number = 0;
	private bonusGameSpins: number = 0;
	private fragmentsFound: number = 0;
	private extraKnuddelByDoublePayoutNormal: number = 0;
	private extraKnuddelByDoublePayoutFreegame: number = 0;
	private threeScatter: number = 0;
	private fourScatter: number = 0;
	private fiveScatter: number = 0;
	private freegamesWonInNormalGame: number = 0;
	private freegamesWonInFreeGame: number = 0;
	private freegamesWonInBonusGame: number = 0;

	private freegamesPerformance: { [key in number]: number } = {
		[0]:0,
		[10]: 0,
		[20]: 0,
		[30]: 0,
		[40]: 0,
		[50]: 0,
		[100]: 0,
		[150]: 0,
		[200]: 0,
		[500]: 0,
		[501]: 0,
	};

	private appearanceFreegameSymbols: { [key in MySlotSymbol]: number } = {
		[MySlotSymbol.K1]: 0,
		[MySlotSymbol.K2]: 0,
		[MySlotSymbol.K3]: 0,
		[MySlotSymbol.K4]: 0,
		[MySlotSymbol.Ace]: 0,
		[MySlotSymbol.King]: 0,
		[MySlotSymbol.Queen]: 0,
		[MySlotSymbol.Jack]: 0,
		[MySlotSymbol.Ten]: 0,
		[MySlotSymbol.Scatter]: 0,
		[MySlotSymbol.Weltreise]: 0,
		[MySlotSymbol.Wild]: 0,
	};

	private amountOfKnuddelWonInFreegamesPerSymbol: { [key in MySlotSymbol]: number } = {
		[MySlotSymbol.K1]: 0,
		[MySlotSymbol.K2]: 0,
		[MySlotSymbol.K3]: 0,
		[MySlotSymbol.K4]: 0,
		[MySlotSymbol.Ace]: 0,
		[MySlotSymbol.King]: 0,
		[MySlotSymbol.Queen]: 0,
		[MySlotSymbol.Jack]: 0,
		[MySlotSymbol.Ten]: 0,
		[MySlotSymbol.Scatter]: 0,
		[MySlotSymbol.Weltreise]: 0,
		[MySlotSymbol.Wild]: 0,
	};

	private appearanceWinWith2KindOfSymbol: { [key in MySlotSymbol]: number } = {
		[MySlotSymbol.K1]: 0,
		[MySlotSymbol.K2]: 0,
		[MySlotSymbol.K3]: 0,
		[MySlotSymbol.K4]: 0,
		[MySlotSymbol.Ace]: 0,
		[MySlotSymbol.King]: 0,
		[MySlotSymbol.Queen]: 0,
		[MySlotSymbol.Jack]: 0,
		[MySlotSymbol.Ten]: 0,
		[MySlotSymbol.Scatter]: 0,
		[MySlotSymbol.Weltreise]: 0,
		[MySlotSymbol.Wild]: 0,
	};

	private appearanceWinWith3KindOfSymbol: { [key in MySlotSymbol]: number } = {
		[MySlotSymbol.K1]: 0,
		[MySlotSymbol.K2]: 0,
		[MySlotSymbol.K3]: 0,
		[MySlotSymbol.K4]: 0,
		[MySlotSymbol.Ace]: 0,
		[MySlotSymbol.King]: 0,
		[MySlotSymbol.Queen]: 0,
		[MySlotSymbol.Jack]: 0,
		[MySlotSymbol.Ten]: 0,
		[MySlotSymbol.Scatter]: 0,
		[MySlotSymbol.Weltreise]: 0,
		[MySlotSymbol.Wild]: 0,
	};

	private appearanceWinWith4KindOfSymbol: { [key in MySlotSymbol]: number } = {
		[MySlotSymbol.K1]: 0,
		[MySlotSymbol.K2]: 0,
		[MySlotSymbol.K3]: 0,
		[MySlotSymbol.K4]: 0,
		[MySlotSymbol.Ace]: 0,
		[MySlotSymbol.King]: 0,
		[MySlotSymbol.Queen]: 0,
		[MySlotSymbol.Jack]: 0,
		[MySlotSymbol.Ten]: 0,
		[MySlotSymbol.Scatter]: 0,
		[MySlotSymbol.Weltreise]: 0,
		[MySlotSymbol.Wild]: 0,
	};

	private appearanceWinWith5KindOfSymbol: { [key in MySlotSymbol]: number } = {
		[MySlotSymbol.K1]: 0,
		[MySlotSymbol.K2]: 0,
		[MySlotSymbol.K3]: 0,
		[MySlotSymbol.K4]: 0,
		[MySlotSymbol.Ace]: 0,
		[MySlotSymbol.King]: 0,
		[MySlotSymbol.Queen]: 0,
		[MySlotSymbol.Jack]: 0,
		[MySlotSymbol.Ten]: 0,
		[MySlotSymbol.Scatter]: 0,
		[MySlotSymbol.Weltreise]: 0,
		[MySlotSymbol.Wild]: 0,
	};

	private appearanceWinWith2KindOfSymbolInFreegames: { [key in MySlotSymbol]: number } = {
		[MySlotSymbol.K1]: 0,
		[MySlotSymbol.K2]: 0,
		[MySlotSymbol.K3]: 0,
		[MySlotSymbol.K4]: 0,
		[MySlotSymbol.Ace]: 0,
		[MySlotSymbol.King]: 0,
		[MySlotSymbol.Queen]: 0,
		[MySlotSymbol.Jack]: 0,
		[MySlotSymbol.Ten]: 0,
		[MySlotSymbol.Scatter]: 0,
		[MySlotSymbol.Weltreise]: 0,
		[MySlotSymbol.Wild]: 0,
	};

	private appearanceWinWith3KindOfSymbolInFreegames: { [key in MySlotSymbol]: number } = {
		[MySlotSymbol.K1]: 0,
		[MySlotSymbol.K2]: 0,
		[MySlotSymbol.K3]: 0,
		[MySlotSymbol.K4]: 0,
		[MySlotSymbol.Ace]: 0,
		[MySlotSymbol.King]: 0,
		[MySlotSymbol.Queen]: 0,
		[MySlotSymbol.Jack]: 0,
		[MySlotSymbol.Ten]: 0,
		[MySlotSymbol.Scatter]: 0,
		[MySlotSymbol.Weltreise]: 0,
		[MySlotSymbol.Wild]: 0,
	};

	private appearanceWinWith4KindOfSymbolInFreegames: { [key in MySlotSymbol]: number } = {
		[MySlotSymbol.K1]: 0,
		[MySlotSymbol.K2]: 0,
		[MySlotSymbol.K3]: 0,
		[MySlotSymbol.K4]: 0,
		[MySlotSymbol.Ace]: 0,
		[MySlotSymbol.King]: 0,
		[MySlotSymbol.Queen]: 0,
		[MySlotSymbol.Jack]: 0,
		[MySlotSymbol.Ten]: 0,
		[MySlotSymbol.Scatter]: 0,
		[MySlotSymbol.Weltreise]: 0,
		[MySlotSymbol.Wild]: 0,
	};

	private appearanceWinWith5KindOfSymbolInFreegames: { [key in MySlotSymbol]: number } = {
		[MySlotSymbol.K1]: 0,
		[MySlotSymbol.K2]: 0,
		[MySlotSymbol.K3]: 0,
		[MySlotSymbol.K4]: 0,
		[MySlotSymbol.Ace]: 0,
		[MySlotSymbol.King]: 0,
		[MySlotSymbol.Queen]: 0,
		[MySlotSymbol.Jack]: 0,
		[MySlotSymbol.Ten]: 0,
		[MySlotSymbol.Scatter]: 0,
		[MySlotSymbol.Weltreise]: 0,
		[MySlotSymbol.Wild]: 0,
	};

	private appearanceOfChestTypes: { [key in ChestType]: number } = {
		[ChestType.Normal]: 0,
		[ChestType.Golden]: 0,
		[ChestType.Giant]: 0,
	};

	private knuddelWonByChestType: { [key in ChestType]: number } = {
		[ChestType.Normal]: 0,
		[ChestType.Golden]: 0,
		[ChestType.Giant]: 0,
	};

	private freegamesWonByChestType: { [key in ChestType]: number } = {
		[ChestType.Normal]: 0,
		[ChestType.Golden]: 0,
		[ChestType.Giant]: 0,
	};
	// ------------------------------------------------------------------------------------------------

	private bonusGameManager: BonusGameManager = new BonusGameManager();
	private slotMachine: MySlotMachine;
	private myInterpreter: MySlotPictureInterpreter;
	private state: P_SlotState;
	private stop: boolean = false;
	private doublePayoutSpins: number = 0;
	private cummulatedFreegamePayout: number = 0;
	private normalSpinsWithoutPayout: number = 0;

	constructor(
		private account: number,
		private buyIn: number,
		private amountOfSpins,
		private onlyFreegames: boolean,
		private onlyFreegamesAmount: number,
		private onlyNormalGames: boolean,
		private disableBonusGames: boolean,
	) {
		this.slotMachine = new MySlotMachine(
			this.createStartConfiguration(),
			FreegameSymbolProbability,
			SLOT_SYMBOL_MAP
		);
		this.startAmount = account;
		this.myInterpreter = this.initializeInterpreter();
		this.initializeState(this.slotMachine.getCurrentPicture());
	}

	simulateSpin() {
		while (!this.stop && this.spinsDone < this.amountOfSpins) {
			if (this.spinsDone % 10000 == 0) {
				console.log(this.spinsDone);
			}
			this.spinsDone++;

			this.handleSpinButtonClick(this.buyIn);
		}

		this.printResult();
	}

	private createStartConfiguration(): MySlotReel[] {
		return [
			new MySlotReel(START_CONFIGURATION, new MySymbolPool(FIRST_REEL_SYMBOL_POOL)),
			new MySlotReel(START_CONFIGURATION, new MySymbolPool(SECOND_REEL_SYMBOL_POOL)),
			new MySlotReel(START_CONFIGURATION, new MySymbolPool(THIRD_REEL_SYMBOL_POOL)),
			new MySlotReel(START_CONFIGURATION, new MySymbolPool(FOURTH_REEL_SYMBOL_POOL)),
			new MySlotReel(START_CONFIGURATION, new MySymbolPool(FIFTH_REEL_SYMBOL_POOL)),
		];
	}

	private initializeInterpreter() {
		return new MySlotPictureInterpreter(
			MY_PAY_LINES,
			MULTIPLIER_MAP,
			SLOT_SYMBOL_MAP[MySlotSymbol.Scatter],
			FREEGAMES_MAP,
			SLOT_SYMBOL_MAP[MySlotSymbol.Wild],
			SLOT_SYMBOL_MAP[MySlotSymbol.Weltreise],
			BONUS_GAME_MAP,
			KEY_FRAGMENT_PROBABILITY
		);
	}

	private initializeState(reels: SlotReel[]) {
		this.state = {
			gameState: GameState.normal,
			slotPhase: SlotPhase.stopped,
			result: {
				fragmentMatrix: null,
				amountOfFragments: 0,
				payout: 0,
				wins: [],
				freeGamePayout: 0,
				bonusGamesWon: 0,
				slotPicture: reels.map(reel => reel.getSymbols()),
				freeGamesWon: 0,
			},
			mostRecentBuyIn: 0,
			knuddelAccount: 0,
			cumulatedPayout: 0,
			freegameState: {
				phase: FreegamePhase.symbolDraw,
				remainingFreeGames: 0,
				freeGameSymbol: null,
				isDoublePayout: false,
			},
			bonusGameState: {
				phase: BonusGamePhase.flying,
				ownPosition: null,
				playerPositions: [],
				remainingFlights: 0,
				amountOfFragments: 0,
				amountOfKeys: 0,
				mostRecentChest: null,
			},
		};
	}

	private startNormalGame(buyInAmount: number) {
		let spinResult: MySlotResult = this.myInterpreter.interpretResult(this.slotMachine.spin(), buyInAmount);
		if(spinResult.payout == 0) {
			this.normalSpinsWithoutPayout++;
		}
		this.trackNormalGameResult2(spinResult);
		this.updateStateAfterSpin(spinResult);
	}

	private startBonusGame(buyInAmount: number) {
		let flightResult: BonusRoundResult = this.bonusGameManager.startRound(this.state.bonusGameState);
		this.trackBonusGameResult(flightResult);
		this.updateStateAfterBonusGame(flightResult, buyInAmount);
	}

	private startFeeGame(buyInAmount: number) {
		this.slotMachine.addSymbol(this.state.freegameState.freeGameSymbol);
		let spinResult: MySlotResult = this.myInterpreter.interpretResultAsFreeGameSpin(
			this.slotMachine.spin(),
			buyInAmount,
			this.state.freegameState.freeGameSymbol
		);
		this.cummulatedFreegamePayout += spinResult.freeGamePayout;
		this.trackFreegameResult2(spinResult);
		this.updateStateAfterFreegame(spinResult);
	}

	private updateStateAfterBonusGame(flightResult: BonusRoundResult, buyInAmount: number) {
		this.updateResult({
			freeGamesWon: flightResult.chestContent.freegames,
			bonusGamesWon: 0,
			wins: [],
			slotPicture: this.state.result.slotPicture,
			freeGamePayout: 0,
			fragmentMatrix: this.state.result.fragmentMatrix,
			payout: flightResult.chestContent.knuddel * buyInAmount,
			amountOfFragments: 0,
		});
		this.updateBonusGameState(0, flightResult);
		this.updateFreeGameState(false, flightResult.chestContent.freegames);
	}

	private updateStateAfterFreegame(spinResult: P_SlotResult) {
		this.state.freegameState.remainingFreeGames--;
		this.updateStateAfterSpin(spinResult);
	}

	private updateStateAfterSpin(spinResult: MySlotResult) {
		//let isDoublePayout: boolean = spinResult.freeGamesWon == FREEGAMES_MAP[5];
		let isDoublePayout: boolean = false;
		if (this.state.freegameState.isDoublePayout) {
			spinResult = this.doublePayout(spinResult);
		}

		this.updateFragments(spinResult);
		this.updateResult(spinResult);
		this.updateBonusGameState(spinResult.bonusGamesWon);
		this.updateFreeGameState(isDoublePayout, spinResult.freeGamesWon);
	}

	private updateFragments(spinResult: P_SlotResult) {
		let fragments: number = spinResult.amountOfFragments + this.state.bonusGameState.amountOfFragments;
		if (fragments >= FRAGMENTS_NEEDED_FOR_KEY) {
			this.state.bonusGameState.amountOfKeys++;
			fragments -= FRAGMENTS_NEEDED_FOR_KEY;
		}
		this.state.bonusGameState.amountOfFragments = fragments;
	}

	private updateResult(spinResult: P_SlotResult) {
		this.state.result = spinResult;
	}

	private updateFreeGameState(isDoublePayout: boolean, wonFreeGames: number) {
		switch (this.state.gameState) {
			case GameState.freeGames:
				this.freegamesWonInFreeGame += wonFreeGames;
				break;
			case GameState.bonusGames:
				this.freegamesWonInBonusGame += wonFreeGames;
				break;
			case GameState.normal:
				this.freegamesWonInNormalGame += wonFreeGames;
				break;
		}
		let isStillDoublePayout: boolean = isDoublePayout;
		let phase: FreegamePhase = this.state.freegameState.phase;
		if (this.state.freegameState.remainingFreeGames == 0 && wonFreeGames == 0) {
			isStillDoublePayout = false;
		}
		if (isDoublePayout) {
			this.state.freegameState.isDoublePayout = true;
		}

		if(this.onlyFreegames && this.state.freegameState.remainingFreeGames>0){
			wonFreeGames = 0;
		}

		this.state.freegameState = {
			remainingFreeGames: this.state.freegameState.remainingFreeGames + wonFreeGames,
			freeGameSymbol: this.state.freegameState.freeGameSymbol,
			isDoublePayout: isStillDoublePayout,
			phase: phase,
		};
	}

	private updateBonusGameState(wonBonusGames: number, flightResult?: BonusRoundResult) {
		let amountOfKeys = this.state.bonusGameState.amountOfKeys;
		if (flightResult && this.state.bonusGameState.amountOfKeys > 0) {
			amountOfKeys--;
		}
		this.state.bonusGameState = {
			phase: BonusGamePhase.flying,
			amountOfKeys: amountOfKeys,
			amountOfFragments: this.state.bonusGameState.amountOfFragments,
			ownPosition: null,
			playerPositions: [],
			remainingFlights: this.disableBonusGames ? 0 : (flightResult
				? this.state.bonusGameState.remainingFlights - 1
				: this.state.bonusGameState.remainingFlights + wonBonusGames),
			mostRecentChest: flightResult ? flightResult.chestType : this.state.bonusGameState.mostRecentChest,
		};
	}

	private updateGameState() {
		if (this.onlyFreegames && this.state.freegameState.remainingFreeGames == 0) {
			this.state.freegameState.remainingFreeGames = this.onlyFreegamesAmount;
		}
		if (this.state.bonusGameState.remainingFlights > 0 && !this.onlyNormalGames && !this.onlyFreegames) {
			this.state.gameState = GameState.bonusGames;
		} else if (this.state.freegameState.remainingFreeGames > 0 && !this.onlyNormalGames) {
			this.state.gameState = GameState.freeGames;
		} else {
			this.state.gameState = GameState.normal;
		}
	}

	private handleSpinButtonClick(buyIn: number) {
		switch (this.state.gameState) {
			case GameState.normal:
				this.handleButtonClickInNormalGame(buyIn);
				break;
			case GameState.bonusGames:
				this.handleButtonClickInBonusGame(buyIn);
				break;
			case GameState.freeGames:
				this.handleButtonClickInFreeGame(buyIn);
				break;
		}
		this.updateGameState();
	}

	private handleButtonClickInNormalGame(buyIn: number) {
		this.startNormalGame(buyIn);
	}

	private handleButtonClickInBonusGame(buyIn: number) {
		this.startBonusGame(buyIn);
	}

	private handleButtonClickInFreeGame(buyIn: number) {
		switch (this.state.freegameState.phase) {
			case FreegamePhase.symbolDraw:
				this.handleFreegameSymbolDraw();
				this.state.freegameState.phase = FreegamePhase.spinning;
				this.handleButtonClickInFreeGame(buyIn);
				break;
			case FreegamePhase.spinning:
				this.startFeeGame(buyIn);
				if (this.state.freegameState.remainingFreeGames == 0) {
					this.trackFreegamePerformace(this.buyIn);
					this.state.freegameState.phase = FreegamePhase.symbolDraw;
				}
		}
	}

	private handleFreegameSymbolDraw() {
		let freeGameSymbol: SlotSymbol = this.slotMachine.drawFreegameSymbol();
		this.state.freegameState.freeGameSymbol = freeGameSymbol;
		this.trackFreeGameSymbol(MySlotSymbol[freeGameSymbol.name]);
	}

	private doublePayout(spinResult: P_SlotResult): P_SlotResult {
		return {
			amountOfFragments: spinResult.amountOfFragments,
			payout: spinResult.payout * 2,
			fragmentMatrix: spinResult.fragmentMatrix,
			freeGamePayout: spinResult.freeGamePayout * 2,
			slotPicture: spinResult.slotPicture,
			wins: spinResult.wins.map(w => {
				return {
					amount: w.amount * 2,
					symbol: w.symbol,
					matches: w.matches,
					winningLine: w.winningLine,
				};
			}),
			bonusGamesWon: spinResult.bonusGamesWon,
			freeGamesWon: spinResult.freeGamesWon,
		};
	}

	private printResult() {
		let knuddelWonInFreeGames: number = this.knuddelWonInFreeGamesWithoutFreegameSymbol;
		let freegamesSymbolKnuddelDistributionHeading: string = '\n';
		let freegamesSymbolKnuddelDistributionContent: string = '\n';
		let appearenceOfFreegameSymbol: string = '\n';
		let appearanceWinWith2KindOfSymbolString: string = '\n';
		let appearanceWinWith3KindOfSymbolString: string = '\n';
		let appearanceWinWith4KindOfSymbolString: string = '\n';
		let appearanceWinWith5KindOfSymbolString: string = '\n';
		let appearanceWinWith2KindOfSymbolInFreegamesString: string = '\n';
		let appearanceWinWith3KindOfSymbolInFreegamesString: string = '\n';
		let appearanceWinWith4KindOfSymbolInFreegamesString: string = '\n';
		let appearanceWinWith5KindOfSymbolInFreegamesString: string = '\n';
		let knuddelWonInBonusGames: number =
			this.knuddelWonByChestType[ChestType.Normal] +
			this.knuddelWonByChestType[ChestType.Golden] +
			this.knuddelWonByChestType[ChestType.Giant];

		for (let key in this.amountOfKnuddelWonInFreegamesPerSymbol) {
			knuddelWonInFreeGames += this.amountOfKnuddelWonInFreegamesPerSymbol[key];
			freegamesSymbolKnuddelDistributionHeading = freegamesSymbolKnuddelDistributionHeading + key + '","';
			appearenceOfFreegameSymbol = appearenceOfFreegameSymbol + this.appearanceFreegameSymbols[key] + '","';
			freegamesSymbolKnuddelDistributionContent =
				freegamesSymbolKnuddelDistributionContent + this.amountOfKnuddelWonInFreegamesPerSymbol[key] + '","';
			appearanceWinWith2KindOfSymbolString =
				appearanceWinWith2KindOfSymbolString + this.appearanceWinWith2KindOfSymbol[key] + '","';
			appearanceWinWith3KindOfSymbolString =
				appearanceWinWith3KindOfSymbolString + this.appearanceWinWith3KindOfSymbol[key] + '","';
			appearanceWinWith4KindOfSymbolString =
				appearanceWinWith4KindOfSymbolString + this.appearanceWinWith4KindOfSymbol[key] + '","';
			appearanceWinWith5KindOfSymbolString =
				appearanceWinWith5KindOfSymbolString + this.appearanceWinWith5KindOfSymbol[key] + '","';
			appearanceWinWith2KindOfSymbolInFreegamesString =
				appearanceWinWith2KindOfSymbolInFreegamesString +
				this.appearanceWinWith2KindOfSymbolInFreegames[key] +
				'","';
			appearanceWinWith3KindOfSymbolInFreegamesString =
				appearanceWinWith3KindOfSymbolInFreegamesString +
				this.appearanceWinWith3KindOfSymbolInFreegames[key] +
				'","';
			appearanceWinWith4KindOfSymbolInFreegamesString =
				appearanceWinWith4KindOfSymbolInFreegamesString +
				this.appearanceWinWith4KindOfSymbolInFreegames[key] +
				'","';
			appearanceWinWith5KindOfSymbolInFreegamesString =
				appearanceWinWith5KindOfSymbolInFreegamesString +
				this.appearanceWinWith5KindOfSymbolInFreegames[key] +
				'","';
		}

		let payIn: number = this.normalSpins * this.buyIn;
		let returnNormalGames: number = (this.knuddelWonInNormalGames / payIn) * 100;
		let returnFreeGames: number = (knuddelWonInFreeGames / payIn) * 100;
		let returnBonusGames: number = (knuddelWonInBonusGames / payIn) * 100;
		let returnTotal: number = returnNormalGames + returnBonusGames + returnFreeGames;
		let returnFreegamesNormalSpins: number = (this.knuddelWonInFreeGamesWithoutFreegameSymbol / payIn) * 100;
		let returnFreegamesFeatureSpins: number = returnFreeGames - returnFreegamesNormalSpins;
		let freegameSpinsFromBonusGame: number =
			this.freegamesWonByChestType[ChestType.Normal] +
			this.freegamesWonByChestType[ChestType.Giant] +
			this.freegamesWonByChestType[ChestType.Golden];
		let estimatedReturnByBonusGameFreegames: number =
			(freegameSpinsFromBonusGame / this.freeGameSpins) * returnFreeGames;
		let estimatedReturnByNormalFreegames: number = returnFreeGames - estimatedReturnByBonusGameFreegames;

		freegamesSymbolKnuddelDistributionHeading = freegamesSymbolKnuddelDistributionHeading + '';

		let golden = '';
		let normal = '';
		let giant = '';
		for(let i = 0 ; i <= 6; i++ ) {
			golden += 'level","' + i + '","content","' + JSON.stringify(GOLDEN_CHEST_LEVEL_TO_CONTENT_MAP[i]) + '","Anzahl:","' + GOLDEN_CHEST_LEVEL_TO_FREQUENCY[i]+ '\n';
			normal += 'level","' + i + '","content","' + JSON.stringify(NORMAL_CHEST_LEVEL_TO_CONTENT_MAP[i]) + '","Anzahl:","' + NORMAL_CHEST_LEVEL_TO_FREQUENCY[i]+ '\n';
			giant += 'level","' + i + '","content","' + JSON.stringify(GIANT_CHEST_LEVEL_TO_CONTENT_MAP[i]) + '","Anzahl:","' + GIANT_CHEST_LEVEL_TO_FREQUENCY[i]+ '\n';
		}

		//Freegamesperformance
		let freegamesPerformance = 'Freegamesperformance:\n' +
			'Auszahlung = 0","' + this.freegamesPerformance[0]/this.timesEnteredFreeGames*100 + '%\n' +
			'Auszahlung <= 10","' + this.freegamesPerformance[10]/this.timesEnteredFreeGames*100 + '%\n' +
			'Auszahlung <= 20","' + this.freegamesPerformance[20]/this.timesEnteredFreeGames*100 + '%\n' +
			'Auszahlung <= 30","' + this.freegamesPerformance[30]/this.timesEnteredFreeGames*100 + '%\n' +
			'Auszahlung <= 40","' + this.freegamesPerformance[40]/this.timesEnteredFreeGames*100 + '%\n' +
			'Auszahlung <= 50","' + this.freegamesPerformance[50]/this.timesEnteredFreeGames*100 + '%\n' +
			'Auszahlung <= 100","' + this.freegamesPerformance[100]/this.timesEnteredFreeGames*100 + '%\n' +
			'Auszahlung <= 150","' + this.freegamesPerformance[150]/this.timesEnteredFreeGames*100 + '%\n' +
			'Auszahlung <= 200","' + this.freegamesPerformance[200]/this.timesEnteredFreeGames*100 + '%\n' +
			'Auszahlung <= 500","' + this.freegamesPerformance[500]/this.timesEnteredFreeGames*100 + '%\n' +
			'Auszahlung > 500","' + this.freegamesPerformance[501]/this.timesEnteredFreeGames*100 + '%\n';

		let output: string = '\n';
		output =
			output +
			'Anzahl der Spins:",' +
			this.spinsDone +
			',"Normal:",' +
			this.normalSpins +
			',"Freispiel:",' +
			this.freeGameSpins +
			',"Bonus","' +
			this.bonusGameSpins +
			'\n' +
			'Spins mit double Payout","' +
			this.doublePayoutSpins +
			'\n' +
			'Normale Spins ohne Payout","' +
			(this.normalSpinsWithoutPayout/this.normalSpins)*100 + '%' +
			'\n' +
			'Guthaben:,' +
			this.account +
			'\n' +
			'Gesammelte Schlüsselfragments:,' +
			this.fragmentsFound +
			'\n' +
			'Anzahl der gestarteten Bonusspiele:,' +
			this.timesEnteredBonusGames +
			'\n' +
			'Anzahl der gestarteten Freipiele:,' +
			this.timesEnteredFreeGames +
			'\n' +
			'Gewonne Knuddel in normalen Spins:,' +
			this.knuddelWonInNormalGames +
			'\n' +
			'In Freispielen gewonnene Knuddel:,' +
			knuddelWonInFreeGames +
			'","Davon mit normalen Spins","' +
			this.knuddelWonInFreeGamesWithoutFreegameSymbol +
			'\n' +
			'"In Bonusspielen gewonnene Knuddel","' +
			knuddelWonInBonusGames +
			'\n' +
			'\n' +
			'\n' +
			'Auszahlung von Knuddel nach Freispielsymbolen","' +
			freegamesSymbolKnuddelDistributionHeading +
			freegamesSymbolKnuddelDistributionContent +
			'\n' +
			'\n' +
			'Häufigkeit von Freispielsymbolen' +
			freegamesSymbolKnuddelDistributionHeading +
			appearenceOfFreegameSymbol +
			'\n' +
			'\n' +
			'\n' +
			'Häufigkeit von Gewinnkombinationen (2er,3er,4er,5er)' +
			freegamesSymbolKnuddelDistributionHeading +
			appearanceWinWith2KindOfSymbolString +
			appearanceWinWith3KindOfSymbolString +
			appearanceWinWith4KindOfSymbolString +
			appearanceWinWith5KindOfSymbolString +
			'\n' +
			'\n' +
			'\n' +
			'Häufigkeit von Scattern\n' +
			'3","4","5' +
			'\n' +
			this.threeScatter +
			'","' +
			this.fourScatter +
			'","' +
			this.fiveScatter +
			'\n' +
			'\n' +
			'\n' +
			'Häufigkeit von Gewinnkombinationen in Freispielen (2er,3er,4er,5er)' +
			freegamesSymbolKnuddelDistributionHeading +
			appearanceWinWith2KindOfSymbolInFreegamesString +
			appearanceWinWith3KindOfSymbolInFreegamesString +
			appearanceWinWith4KindOfSymbolInFreegamesString +
			appearanceWinWith5KindOfSymbolInFreegamesString +
			'\n' +
			'\n' +
			'\n' +
			'\n' +
			'Truhentypen' +
			'","' +
			'Normal' +
			'","' +
			'Golden' +
			'","' +
			'Riesen' +
			'\n' +
			'Häufigkeit' +
			'","' +
			this.appearanceOfChestTypes[ChestType.Normal] +
			'","' +
			this.appearanceOfChestTypes[ChestType.Golden] +
			'","' +
			this.appearanceOfChestTypes[ChestType.Giant] +
			'\n' +
			'\n' +
			normal +
			'\n' +
			golden +
			'\n' +
			giant +
			'\n' +
			'Gewonnene Knuddel' +
			'","' +
			this.knuddelWonByChestType[ChestType.Normal] +
			'","' +
			this.knuddelWonByChestType[ChestType.Golden] +
			'","' +
			this.knuddelWonByChestType[ChestType.Giant] +
			'\n' +
			'Gewonnene Freispiele' +
			'","' +
			this.freegamesWonByChestType[ChestType.Normal] +
			'","' +
			this.freegamesWonByChestType[ChestType.Golden] +
			'","' +
			this.freegamesWonByChestType[ChestType.Giant] +
			'\n' +
			'\n' +
			'Return Total","' +
			returnTotal +
			'%' +
			'\n' +
			'Normalspiel","' +
			returnNormalGames +
			'%' +
			'\n' +
			'Freispiele","' +
			returnFreeGames +
			'%' +
			'\n' +
			'Bonusspiele(nur Knuddel)","' +
			returnBonusGames +
			'%' +
			'\n' +
			'Freispiele normale","' +
			returnFreegamesNormalSpins +
			'%' +
			'\n' +
			'Freispiele Sonderdrehs","' +
			returnFreegamesFeatureSpins +
			'%' +
			'\n' +
			'Freispiele durch normale Drehs","' +
			estimatedReturnByNormalFreegames +
			'%' +
			'\n' +
			'Freispiele durch Bonusspiele","' +
			estimatedReturnByBonusGameFreegames +
			'%' +
			'\n' +
			'Zusätzlich durch doppelte Auszahlung erhaltene Knuddel","' +
			this.extraKnuddelByDoublePayoutNormal +
			'","normal","' +
			this.extraKnuddelByDoublePayoutFreegame +
			'","freegame' +
			'\n' +freegamesPerformance+
		'\n' +
		'freespins won in normalgame","' +
			this.freegamesWonInNormalGame +
		'\n' +
		'freespins won in freegame","' +
			this.freegamesWonInFreeGame +
		'\n' +
		'freespins won in bonusgame","' +
			this.freegamesWonInBonusGame;

		output = this.convertToCSV(output);

		console.log('Ergebniss nach ' + this.spinsDone + ' Spins', output);
	}

	// ------- TRACKING --------

	trackWinCombination(amount: number, symbol: MySlotSymbol) {
		if (amount == 2) {
			this.appearanceWinWith2KindOfSymbol[symbol]++;
		} else if (amount == 3) {
			this.appearanceWinWith3KindOfSymbol[symbol]++;
		} else if (amount == 4) {
			this.appearanceWinWith4KindOfSymbol[symbol]++;
		} else if (amount == 5) {
			this.appearanceWinWith5KindOfSymbol[symbol]++;
		}
	}

	trackWinCombinationInFreegames(amount: number, symbol: MySlotSymbol) {
		if (amount == 2) {
			this.appearanceWinWith2KindOfSymbolInFreegames[symbol]++;
		} else if (amount == 3) {
			this.appearanceWinWith3KindOfSymbolInFreegames[symbol]++;
		} else if (amount == 4) {
			this.appearanceWinWith4KindOfSymbolInFreegames[symbol]++;
		} else if (amount == 5) {
			this.appearanceWinWith5KindOfSymbolInFreegames[symbol]++;
		}
	}

	trackFreeGameSymbol(symbol: MySlotSymbol) {
		this.appearanceFreegameSymbols[symbol]++;
		this.increaseFreegamesEntered();
	}

	trackBonusGame(chestType: ChestType, knuddelWon: number, amountOfFreegamesWon: number) {
		this.appearanceOfChestTypes[chestType]++;
		this.knuddelWonByChestType[chestType] += knuddelWon;
		this.freegamesWonByChestType[chestType] += amountOfFreegamesWon;
		this.account += knuddelWon;
		this.bonusGameSpins++;
	}

	trackFreegameResult(freegameSymbol: MySlotSymbol, spinResult: P_SlotResult, isDoublePayout) {
		if (isDoublePayout) {
			this.extraKnuddelByDoublePayoutNormal += spinResult.payout;
			this.extraKnuddelByDoublePayoutFreegame += spinResult.freeGamePayout;
			this.doublePayoutSpins++;
		}
		let mulitplier: number = isDoublePayout ? 2 : 1;
		this.amountOfKnuddelWonInFreegamesPerSymbol[freegameSymbol] += spinResult.freeGamePayout * mulitplier;
		this.knuddelWonInFreeGamesWithoutFreegameSymbol += spinResult.payout * mulitplier;
		this.account += spinResult.freeGamePayout + spinResult.payout;
		this.freeGameSpins++;
	}

	trackNormalGameResult(knuddelWon: number) {
		this.knuddelWonInNormalGames += knuddelWon;
		this.account += knuddelWon - this.buyIn;
		this.normalSpins++;
	}

	increaseFreegamesEntered() {
		this.timesEnteredFreeGames++;
	}

	increaseCountOfBonusGameEntrances() {
		this.timesEnteredBonusGames++;
	}

	stopSimulation() {
		this.stop = true;
	}

	trackFragments(keyFragmentsFound: number) {
		this.fragmentsFound += keyFragmentsFound;
	}

	private trackNormalGameResult2(spinResult: MySlotResult) {
		this.trackNormalGameResult(spinResult.payout);
		spinResult.wins.forEach(w => this.trackWinCombination(w.matches, MySlotSymbol[w.symbol.name]));
		this.trackFragments(spinResult.amountOfFragments);
		if (spinResult.freeGamesWon == FREEGAMES_MAP[3]) {
			this.threeScatter++;
		}
		if (spinResult.freeGamesWon == FREEGAMES_MAP[4]) {
			this.fourScatter++;
		}
		if (spinResult.freeGamesWon == FREEGAMES_MAP[5]) {
			this.fiveScatter++;
		}
	}

	private trackBonusGameResult(flightResult: BonusRoundResult) {
		if(this.state.bonusGameState.remainingFlights == 1) {
			this.increaseCountOfBonusGameEntrances();
		}
		this.trackBonusGame(
			flightResult.chestType,
			flightResult.chestContent.knuddel,
			flightResult.chestContent.freegames
		);
	}

	private trackFreegameResult2(spinResult: MySlotResult) {
		this.trackFreegameResult(
			MySlotSymbol[this.state.freegameState.freeGameSymbol.name],
			spinResult,
			this.state.freegameState.isDoublePayout
		);
		spinResult.wins.forEach(w => this.trackWinCombinationInFreegames(w.matches, MySlotSymbol[w.symbol.name]));
	}

	private convertToCSV(output: string): string {
		let returnValue: string = output.split('\n').join('"\n"');
		returnValue = returnValue.split(':,').join(':","');
		returnValue = returnValue.split('.').join(',');
		return returnValue;
	}

	private trackFreegamePerformace(buyIn: number) {
		let factor = this.cummulatedFreegamePayout;
		if(factor == 0) {
			this.freegamesPerformance[0] += 1;
		} else if(factor <= 10) {
			this.freegamesPerformance[10] += 1;
		} else if(factor <= 20) {
			this.freegamesPerformance[20] += 1;
		} else if(factor <= 30) {
			this.freegamesPerformance[30] += 1;
		} else if(factor <= 40) {
			this.freegamesPerformance[40] += 1;
		} else if(factor <= 50) {
			this.freegamesPerformance[50] += 1;
		} else if(factor <= 100) {
			this.freegamesPerformance[100] += 1;
		} else if(factor <= 150) {
			this.freegamesPerformance[150] += 1;
		} else if(factor <= 200) {
			this.freegamesPerformance[200] += 1;
		} else if(factor <= 500) {
			this.freegamesPerformance[500] += 1;
		} else {
			this.freegamesPerformance[501] += 1;
		}

		this.cummulatedFreegamePayout = 0;
	}
}
