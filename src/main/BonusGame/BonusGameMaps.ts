export interface ChestContent {
	knuddel: number;
	freegames: number;
}

export const NORMAL_CHEST_LEVEL_TO_CONTENT_MAP: { [key in number]: ChestContent } = {
	0: { knuddel: 2, freegames: 0 },
	1: { knuddel: 3, freegames: 0 },
	2: { knuddel: 4, freegames: 0 },
	3: { knuddel: 5, freegames: 0 },
	4: { knuddel: 7, freegames: 0 },
	5: { knuddel: 10, freegames: 0 },
	6: { knuddel: 20, freegames: 0 },
};

export const GOLDEN_CHEST_LEVEL_TO_CONTENT_MAP: { [key in number]: ChestContent } = {
	0: { knuddel: 5, freegames: 0 },
	1: { knuddel: 7, freegames: 0 },
	2: { knuddel: 10, freegames: 0 },
	3: { knuddel: 15, freegames: 0 },
	4: { knuddel: 20, freegames: 0 },
	5: { knuddel: 25, freegames: 0 },
	6: { knuddel: 50, freegames: 0 },
};

export const GIANT_CHEST_LEVEL_TO_CONTENT_MAP: { [key in number]: ChestContent } = {
	0: { knuddel: 30, freegames: 0 },
	1: { knuddel: 40, freegames: 0 },
	2: { knuddel: 50, freegames: 0 },
	3: { knuddel: 75, freegames: 0 },
	4: { knuddel: 100, freegames: 0},
	5: { knuddel: 250, freegames: 0 },
	6: { knuddel: 500, freegames: 0 },
};

export const NORMAL_CHEST_LEVEL_TO_PROBABILITY_MAP: { [key in number]: number } = {
	0: 346,
	1: 300,
	2: 218,
	3: 100,
	4: 25,
	5: 10,
	6: 1,
};

export const GOLDEN_CHEST_LEVEL_TO_PROBABILITY_MAP: { [key in number]: number } = {
	0: 100,
	1: 250,
	2: 350,
	3: 250,
	4: 43,
	5: 5,
	6: 2,
};

export const GIANT_CHEST_LEVEL_TO_PROBABILITY_MAP: { [key in number]: number } = {
	0: 200,
	1: 250,
	2: 250,
	3: 240,
	4: 40,
	5: 15,
	6: 5,
};

