import { ChestType, P_BonusGameState } from '../protocol';
import {
	ChestContent,
	GIANT_CHEST_LEVEL_TO_CONTENT_MAP,
	GIANT_CHEST_LEVEL_TO_PROBABILITY_MAP,
	GOLDEN_CHEST_LEVEL_TO_CONTENT_MAP,
	GOLDEN_CHEST_LEVEL_TO_PROBABILITY_MAP,
	NORMAL_CHEST_LEVEL_TO_CONTENT_MAP,
	NORMAL_CHEST_LEVEL_TO_PROBABILITY_MAP,
} from './BonusGameMaps';


export interface BonusRoundResult {
	chestType: ChestType;
	chestContent: ChestContent;
}

const GIANT_CHEST_OFFSET: number = 1580;

export const NORMAL_CHEST_LEVEL_TO_FREQUENCY: { [key in number]: number } = {
	0: 0,
	1: 0,
	2: 0,
	3: 0,
	4: 0,
	5: 0,
	6: 0,
};

export const GOLDEN_CHEST_LEVEL_TO_FREQUENCY: { [key in number]: number } = {
	0: 0,
	1: 0,
	2: 0,
	3: 0,
	4: 0,
	5: 0,
	6: 0,
};

export const GIANT_CHEST_LEVEL_TO_FREQUENCY: { [key in number]: number } = {
	0: 0,
	1: 0,
	2: 0,
	3: 0,
	4: 0,
	5: 0,
	6: 0,
};

export class BonusGameManager {
	private flightsSinceLastGiantChest: number = 0;
	constructor() {}

	public startRound(bonusGameState: P_BonusGameState): BonusRoundResult {
		let result: BonusRoundResult = {
			chestType: null,
			chestContent: null,
		};

		let amountOfGoldenKeys: number = bonusGameState.amountOfKeys;
		let giantChestChance: number =
			this.flightsSinceLastGiantChest / (GIANT_CHEST_OFFSET + this.flightsSinceLastGiantChest);
		if (Math.random() < giantChestChance) {
			result.chestType = ChestType.Giant;
			result.chestContent = this.openGiantChest();
			this.flightsSinceLastGiantChest = 0;
		} else if (amountOfGoldenKeys > 0) {
			result.chestType = ChestType.Golden;
			result.chestContent = this.openGoldenChest();
		} else {
			result.chestType = ChestType.Normal;
			result.chestContent = this.openNormalChest();
		}
		this.flightsSinceLastGiantChest++;
		return result;
	}

	private openNormalChest(): ChestContent {
		let lostopf: number[] = [];
		for (let key in NORMAL_CHEST_LEVEL_TO_PROBABILITY_MAP) {
			for (let i = 0; i < NORMAL_CHEST_LEVEL_TO_PROBABILITY_MAP[key]; i++) {
				lostopf.push(Number(key));
			}
		}
		return this.drawChestContent(ChestType.Normal, lostopf);
	}

	private openGoldenChest() {
		let lostopf: number[] = [];
		for (let key in GOLDEN_CHEST_LEVEL_TO_PROBABILITY_MAP) {
			for (let i = 0; i < GOLDEN_CHEST_LEVEL_TO_PROBABILITY_MAP[key]; i++) {
				lostopf.push(Number(key));
			}
		}
		return this.drawChestContent(ChestType.Golden, lostopf);
	}

	private openGiantChest() {
		let lostopf: number[] = [];
		for (let key in GIANT_CHEST_LEVEL_TO_PROBABILITY_MAP) {
			for (let i = 0; i < GIANT_CHEST_LEVEL_TO_PROBABILITY_MAP[key]; i++) {
				lostopf.push(Number(key));
			}
		}

		return this.drawChestContent(ChestType.Giant, lostopf);
	}

	private drawChestContent(chestType: ChestType, lostopf: number[]): ChestContent {
		let contentMap: { [key in number]: ChestContent };
		let frequencyMap: { [key in number]: number };
		if (chestType == ChestType.Giant) {
			contentMap = GIANT_CHEST_LEVEL_TO_CONTENT_MAP;
			frequencyMap = GIANT_CHEST_LEVEL_TO_FREQUENCY;
		} else if (chestType == ChestType.Golden) {
			contentMap = GOLDEN_CHEST_LEVEL_TO_CONTENT_MAP;
			frequencyMap = GOLDEN_CHEST_LEVEL_TO_FREQUENCY;
		} else {
			contentMap = NORMAL_CHEST_LEVEL_TO_CONTENT_MAP;
			frequencyMap = NORMAL_CHEST_LEVEL_TO_FREQUENCY;
		}
		let random = lostopf[Math.floor(Math.random() * lostopf.length)];
		frequencyMap[random]++;
		return contentMap[random];
	}
}
