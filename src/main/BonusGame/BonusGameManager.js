"use strict";
exports.__esModule = true;
var protocol_1 = require("../protocol");
var BonusGameMaps_1 = require("./BonusGameMaps");
var GIANT_CHEST_OFFSET = 1580;
exports.NORMAL_CHEST_LEVEL_TO_FREQUENCY = {
    0: 0,
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0
};
exports.GOLDEN_CHEST_LEVEL_TO_FREQUENCY = {
    0: 0,
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0
};
exports.GIANT_CHEST_LEVEL_TO_FREQUENCY = {
    0: 0,
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0
};
var BonusGameManager = /** @class */ (function () {
    function BonusGameManager() {
        this.flightsSinceLastGiantChest = 0;
    }
    BonusGameManager.prototype.startRound = function (bonusGameState) {
        var result = {
            chestType: null,
            chestContent: null
        };
        var amountOfGoldenKeys = bonusGameState.amountOfKeys;
        var giantChestChance = this.flightsSinceLastGiantChest / (GIANT_CHEST_OFFSET + this.flightsSinceLastGiantChest);
        if (Math.random() < giantChestChance) {
            result.chestType = protocol_1.ChestType.Giant;
            result.chestContent = this.openGiantChest();
            this.flightsSinceLastGiantChest = 0;
        }
        else if (amountOfGoldenKeys > 0) {
            result.chestType = protocol_1.ChestType.Golden;
            result.chestContent = this.openGoldenChest();
        }
        else {
            result.chestType = protocol_1.ChestType.Normal;
            result.chestContent = this.openNormalChest();
        }
        this.flightsSinceLastGiantChest++;
        return result;
    };
    BonusGameManager.prototype.openNormalChest = function () {
        var lostopf = [];
        for (var key in BonusGameMaps_1.NORMAL_CHEST_LEVEL_TO_PROBABILITY_MAP) {
            for (var i = 0; i < BonusGameMaps_1.NORMAL_CHEST_LEVEL_TO_PROBABILITY_MAP[key]; i++) {
                lostopf.push(Number(key));
            }
        }
        return this.drawChestContent(protocol_1.ChestType.Normal, lostopf);
    };
    BonusGameManager.prototype.openGoldenChest = function () {
        var lostopf = [];
        for (var key in BonusGameMaps_1.GOLDEN_CHEST_LEVEL_TO_PROBABILITY_MAP) {
            for (var i = 0; i < BonusGameMaps_1.GOLDEN_CHEST_LEVEL_TO_PROBABILITY_MAP[key]; i++) {
                lostopf.push(Number(key));
            }
        }
        return this.drawChestContent(protocol_1.ChestType.Golden, lostopf);
    };
    BonusGameManager.prototype.openGiantChest = function () {
        var lostopf = [];
        for (var key in BonusGameMaps_1.GIANT_CHEST_LEVEL_TO_PROBABILITY_MAP) {
            for (var i = 0; i < BonusGameMaps_1.GIANT_CHEST_LEVEL_TO_PROBABILITY_MAP[key]; i++) {
                lostopf.push(Number(key));
            }
        }
        return this.drawChestContent(protocol_1.ChestType.Giant, lostopf);
    };
    BonusGameManager.prototype.drawChestContent = function (chestType, lostopf) {
        var contentMap;
        var frequencyMap;
        if (chestType == protocol_1.ChestType.Giant) {
            contentMap = BonusGameMaps_1.GIANT_CHEST_LEVEL_TO_CONTENT_MAP;
            frequencyMap = exports.GIANT_CHEST_LEVEL_TO_FREQUENCY;
        }
        else if (chestType == protocol_1.ChestType.Golden) {
            contentMap = BonusGameMaps_1.GOLDEN_CHEST_LEVEL_TO_CONTENT_MAP;
            frequencyMap = exports.GOLDEN_CHEST_LEVEL_TO_FREQUENCY;
        }
        else {
            contentMap = BonusGameMaps_1.NORMAL_CHEST_LEVEL_TO_CONTENT_MAP;
            frequencyMap = exports.NORMAL_CHEST_LEVEL_TO_FREQUENCY;
        }
        var random = lostopf[Math.floor(Math.random() * lostopf.length)];
        frequencyMap[random]++;
        return contentMap[random];
    };
    return BonusGameManager;
}());
exports.BonusGameManager = BonusGameManager;
